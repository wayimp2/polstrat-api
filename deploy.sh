#!/usr/bin/env bash

npm test
npm run prebuild
npm run build --production
docker-compose down --rmi local
docker-compose up