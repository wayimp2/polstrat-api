import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { User } from "../../../src/models"
import { EMAIL_SECRET } from "../../../src/config"
import searchUserFilter from "../../dummies/searchUserFilter.json"
import communityDummy from "../../dummies/communityDummy.json"
import searchCommunityFilter from "../../dummies/searchCommunityFilter.json"
import userAdummy from "../../dummies/dummyUsers/userAdummy.json"
import userBdummy from "../../dummies/dummyUsers/userBdummy.json"
import userCdummy from "../../dummies/dummyUsers/userCdummy.json"
//import util from "util"

const expect = chai.expect
const urlBase = "/api/v1/search"

describe("SEARCH", () => {
  //ex. userAdummy => usersDummies[0][0] | user => usersDummies[0][1] | token => usersDummies[0][2]
  const usersDummies = [[userAdummy], [userBdummy], [userCdummy]]
  const userA = usersDummies[0]
  const userB = usersDummies[1]
  const userC = usersDummies[2]

  // CREATE USERS

  before(async () => {
    // Create users and authenticate them
    try {
      for (let i = 0; i < usersDummies.length; i++) {
        let userRes = await request(app)
          .post(`/api/v1/user/create`)
          .send(usersDummies[i][0])

        const createdUser = await User.findById(userRes.body.id)
        usersDummies[i].push(createdUser)

        const confirmToken = jwt.sign({ id: createdUser.id }, EMAIL_SECRET, {
          expiresIn: "1d",
        })

        await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

        const AuthRes = await request(app)
          .post(`/api/v1/user/authenticate`)
          .send({
            username: usersDummies[i][0].username,
            password: usersDummies[i][0].password,
          })

        usersDummies[i].push(AuthRes.body.token)
      }

      await request(app)
        .post(`/api/v1/community/create`)
        .send(communityDummy)
        .set("Authorization", `Bearer ${userA[2]}`)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // USER FILTERS

  it("POST / It should retrieve a user with the search user filters parameters", async () => {
    try {
      const searchUser = await request(app)
        .post(`${urlBase}/filter/user`)
        .send(searchUserFilter)
        .set("Authorization", `Bearer ${userA[2]}`)

      //expect(searchUser.body).to.be.an("array").to.have.lengthOf(1)
      expect(searchUser.status).to.equal(200)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // COMMUNITY FILTERS

  it("POST / It should retrieve a community with the search user filters parameters", async () => {
    try {
      const searchCommunity = await request(app)
        .post(`${urlBase}/filter/community`)
        .send(searchCommunityFilter)
        .set("Authorization", `Bearer ${userA[2]}`)

      //console.log(searchCommunity.body)

      //expect(searchCommunity.body).to.be.an("array").to.have.lengthOf(1)
      expect(searchCommunity.status).to.equal(200)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // TEXT SEARCH

  it("GET / It should retrieve 3 users and 1 community with the search text parameters", async () => {
    try {
      const searchText = await request(app)
        .get(`${urlBase}/text?search=user`)
        .set("Authorization", `Bearer ${userA[2]}`)

      expect(searchText.status).to.equal(200)

      /*
    console.log(
      util.inspect(searchText.body, { showHidden: false, depth: null })
    )
    */

      //expect(searchText.body.users).to.be.an("array").to.have.lengthOf(3)
      //expect(searchText.body.communities).to.be.an("array").to.have.lengthOf(1)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  after(async () => {
    // Remove all users
    const users = [userA, userB, userC]

    try {
      for (let i = 0; i < users.length; i++) {
        await request(app)
          .delete(`/api/v1/user/delete/${users[i][1].id}`)
          .set("Authorization", `Bearer ${users[i][2]}`)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
