import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { User } from "../../../src/models"
import { EMAIL_SECRET } from "../../../src/config"
import userAdummy from "../../dummies/dummyUsers/userAdummy.json"
import userBdummy from "../../dummies/dummyUsers/userBdummy.json"
import userCdummy from "../../dummies/dummyUsers/userCdummy.json"
//import util from "util"

const expect = chai.expect
const urlBase = "/api/v1"

describe("VISIBILITY", () => {
  //ex. userAdummy => usersDummies[0][0] | user => usersDummies[0][1] | token => usersDummies[0][2]
  const usersDummies = [[userAdummy], [userBdummy], [userCdummy]]
  const userA = usersDummies[0]
  const userB = usersDummies[1]
  const userC = usersDummies[2]

  // CREATE USERS

  before(async () => {
    // Create users and authenticate them
    try {
      for (let i = 0; i < usersDummies.length; i++) {
        let userRes = await request(app)
          .post(`/api/v1/user/create`)
          .send(usersDummies[i][0])

        const createdUser = await User.findById(userRes.body.id)
        usersDummies[i].push(createdUser)

        const confirmToken = jwt.sign({ id: createdUser.id }, EMAIL_SECRET, {
          expiresIn: "1d",
        })

        await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

        const AuthRes = await request(app)
          .post(`/api/v1/user/authenticate`)
          .send({
            username: usersDummies[i][0].username,
            password: usersDummies[i][0].password,
          })

        usersDummies[i].push(AuthRes.body.token)
      }

      // request friend
      const requestConnection = await request(app)
        .post(`${urlBase}/user/connection/request/${userB[1].id}`)
        .set("Authorization", `Bearer ${userA[2]}`)

      // accept friend

      await request(app)
        .put(`${urlBase}/user/connection/approve/${requestConnection.body.id}`)
        .set("Authorization", `Bearer ${userB[2]}`)

      //create community
      await request(app)
        .post(`/api/v1/community/create`)
        .send({
          name: "user",
        })
        .set("Authorization", `Bearer ${userA[2]}`)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // TODO: do more cases
  it("GET / It should retrieve a user only with the visibility options", async () => {
    try {
      const resGetById = await request(app)
        .get(`${urlBase}/user/id/${userB[1].id}`)
        .set("Authorization", `Bearer ${userA[2]}`)

      expect(resGetById.status).to.equal(200)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  after(async () => {
    // Remove all users
    const users = [userA, userB, userC]

    try {
      for (let i = 0; i < users.length; i++) {
        await request(app)
          .delete(`/api/v1/user/delete/${users[i][1].id}`)
          .set("Authorization", `Bearer ${users[i][2]}`)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
