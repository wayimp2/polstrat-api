import request from "supertest"
import chai from "chai"
import app from "../../../src/app"
//import database from "../../../database"

describe("API STATE", () => {
  /*
  before(async () => {
    await database.connection.db.dropDatabase()
    console.log("Database dropped successfully")
  })
  */

  it(" GET / API V1 health check'", async () => {
    try {
      await request(app).get("/api/v1/health-check").expect(200)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
