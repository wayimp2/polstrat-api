import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { EMAIL_SECRET, RESET_PASSWORD_SECRET } from "../../../src/config"
import { User } from "../../../src/models"
import userDummy from "../../dummies/dummyUsers/userDummy.json"

const expect = chai.expect
const urlBase = "/api/v1/user"

describe("USER ACTIONS", () => {
  let userId = null
  let token = null
  let testEmail = userDummy.email

  it("POST / It should create a user and send email for confirmation", async () => {
    try {
      const res = await request(app).post(`${urlBase}/create`).send(userDummy)

      userId = res.body.id
      const createdUser = await User.findById(userId)

      expect(res.status).to.equal(200)
      expect(createdUser).to.have.a.property("_id")
      expect(createdUser.confirmed).to.equal(false)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("GET / It should confirm the account in email", async () => {
    try {
      const confirmToken = jwt.sign({ id: userId }, EMAIL_SECRET, {
        expiresIn: "1d",
      })

      const res = await request(app).get(
        `${urlBase}/confirmation/${confirmToken}`
      )

      expect(res.status).to.equal(302)

      const confirmedUser = await User.findById(userId)

      expect(confirmedUser.confirmed).to.equal(true)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("POST / It should authenticate the user by username", async () => {
    const { username } = userDummy
    const { password } = userDummy

    try {
      const res = await request(app).post(`${urlBase}/authenticate`).send({
        username,
        password,
      })

      token = res.body.token

      expect(res.status).to.equal(200)
      expect(res.body).to.have.a.property("token")
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("POST / It should authenticate the user by email", async () => {
    const { email } = userDummy
    const { password } = userDummy

    try {
      const res = await request(app).post(`${urlBase}/authenticate`).send({
        email,
        password,
      })

      token = res.body.token

      expect(res.status).to.equal(200)
      expect(res.body).to.have.a.property("token")
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("PUT / It should update a user", async () => {
    const newUsername = "newUsernameUpdated"

    try {
      const res = await request(app)
        .put(`${urlBase}/update/${userId}`)
        .send({ username: newUsername })
        .set("Authorization", `Bearer ${token}`)

      const updatedUser = await User.findById(userId)

      expect(res.status).to.equal(200)
      expect(updatedUser.username).to.equal(newUsername)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("POST / It should reset the password and authenticate with the new password", async () => {
    const testNewPassword = "newPasswordUpdated"

    try {
      const resResetPassword = await request(app)
        .post(`${urlBase}/reset-password`)
        .send({ email: testEmail })

      expect(resResetPassword.status).to.equal(200)

      const resetPasswordToken = jwt.sign(
        { email: testEmail },
        RESET_PASSWORD_SECRET,
        {
          expiresIn: "1d",
        }
      )

      const resChangePassword = await request(app)
        .put(`${urlBase}/change-password/${resetPasswordToken}`)
        .send({ password: testNewPassword, confirmPassword: testNewPassword })

      expect(resChangePassword.status).to.equal(302)

      const resAuthenticate = await request(app)
        .post(`${urlBase}/authenticate`)
        .send({
          email: testEmail,
          password: testNewPassword,
        })

      expect(resAuthenticate.status).to.equal(200)
      expect(resAuthenticate.body).to.have.a.property("token")
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("GET / It should retrieve the current user", async () => {
    try {
      const resCurrentUser = await request(app)
        .get(`${urlBase}/current`)
        .set("Authorization", `Bearer ${token}`)

      expect(resCurrentUser.status).to.equal(200)
      expect(resCurrentUser.body).to.have.a.property("_id")
      expect(resCurrentUser.body._id.toString()).to.equal(userId)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // TODO: get user by username
  /*
  it("GET / It should retrieve a user by id", async () => {
    try {
      const resGetById = await request(app)
        .get(`${urlBase}/id/${userId}`)
        .set("Authorization", `Bearer ${token}`)

      expect(resGetById.status).to.equal(200)

      const userById = await User.findById(userId)

      expect(resGetById.body).to.have.a.property("_id")
      expect(resGetById.body._id.toString()).to.equal(userById._id.toString())
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
  */

  it("DELETE / It should delete a user", async () => {
    try {
      const resDeletedUser = await request(app)
        .delete(`${urlBase}/delete/${userId}`)
        .set("Authorization", `Bearer ${token}`)

      const deletedUser = await User.findById(userId)

      expect(resDeletedUser.status).to.equal(200)
      expect(deletedUser).to.equal(null)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
