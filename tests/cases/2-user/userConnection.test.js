import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { UserConnection, User } from "../../../src/models"
import { EMAIL_SECRET } from "../../../src/config"
import userAdummy from "../../dummies/dummyUsers/userAdummy.json"
import userBdummy from "../../dummies/dummyUsers/userBdummy.json"
import userCdummy from "../../dummies/dummyUsers/userCdummy.json"

const expect = chai.expect
const urlBase = "/api/v1/user/connection"

describe("USER CONNECTIONS", () => {
  //ex. userAdummy => usersDummies[0][0] | user => usersDummies[0][1] | token => usersDummies[0][2]
  const usersDummies = [[userAdummy], [userBdummy], [userCdummy]]
  const userA = usersDummies[0]
  const userB = usersDummies[1]
  const userC = usersDummies[2]
  let userConnectionId = null
  let blockedUserConnectionId = null

  // CREATE USERS

  before(async () => {
    // Create users and authenticate them
    try {
      for (let i = 0; i < usersDummies.length; i++) {
        let userRes = await request(app)
          .post(`/api/v1/user/create`)
          .send(usersDummies[i][0])

        const createdUser = await User.findById(userRes.body.id)
        usersDummies[i].push(createdUser)

        const confirmToken = jwt.sign({ id: createdUser.id }, EMAIL_SECRET, {
          expiresIn: "1d",
        })

        await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

        const AuthRes = await request(app)
          .post(`/api/v1/user/authenticate`)
          .send({
            username: usersDummies[i][0].username,
            password: usersDummies[i][0].password,
          })

        usersDummies[i].push(AuthRes.body.token)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // REQUEST CONNECTION A TO B

  it("POST / It should request a connection from userA to userB", async () => {
    const requestConnection = await request(app)
      .post(`${urlBase}/request/${userB[1].id}`)
      .set("Authorization", `Bearer ${userA[2]}`)

    expect(requestConnection.status).to.equal(200)

    userConnectionId = requestConnection.body.id

    const userConnection = await UserConnection.findById(userConnectionId)

    expect(userConnection).to.have.a.property("_id")
    expect(userConnection.requester.toString()).to.equal(userA[1].id)
    expect(userConnection.recipient.toString()).to.equal(userB[1].id)
    expect(userConnection.status).to.equal(1)
  })

  // APPROVE CONNECTION B TO A

  it("PUT / It should approve the requested connection from the userA to userB", async () => {
    const approveConnection = await request(app)
      .put(`${urlBase}/approve/${userConnectionId}`)
      .set("Authorization", `Bearer ${userB[2]}`)

    expect(approveConnection.status).to.equal(200)

    const userConnection = await UserConnection.findById(userConnectionId)

    expect(userConnection.status).to.equal(2)
  })

  // LIST ALL CONNECTION OF A

  it("GET / It should list all connections of the user", async () => {
    const userMyConnections = await request(app)
      .get(`${urlBase}/my`)
      .set("Authorization", `Bearer ${userA[2]}`)

    expect(userMyConnections.status).to.equal(200)
    expect(userMyConnections.body).to.be.an("array").to.have.lengthOf(1)
    expect(userMyConnections.body[0].username).to.equal("userB")
  })

  // REQUEST CONNECTION FROM C TO B

  it("POST / It should request a connection from userC to userB", async () => {
    const requestConnection = await request(app)
      .post(`${urlBase}/request/${userB[1].id}`)
      .set("Authorization", `Bearer ${userC[2]}`)

    expect(requestConnection.status).to.equal(200)
  })

  // LIST ALL REQUESTED CONNECTIONS TO B

  it("GET / It should list all requested connections to userB", async () => {
    const userMyRequested = await request(app)
      .get(`${urlBase}/requests`)
      .set("Authorization", `Bearer ${userB[2]}`)

    expect(userMyRequested.status).to.equal(200)
    expect(userMyRequested.body).to.be.an("array").to.have.lengthOf(1)
    expect(userMyRequested.body[0].username).to.equal("userC")
  })

  // LIST ALL PENDING CONNECTIONS

  it("GET / It should list all pending connections to userC", async () => {
    const userMyPendings = await request(app)
      .get(`${urlBase}/pending`)
      .set("Authorization", `Bearer ${userC[2]}`)

    expect(userMyPendings.status).to.equal(200)
    expect(userMyPendings.body).to.be.an("array").to.have.lengthOf(1)
    expect(userMyPendings.body[0].username).to.equal("userB")
  })

  // BLOCK CONNECTION C TO B

  it("POST / It should block a userB form userC", async () => {
    const blockConnection = await request(app)
      .post(`${urlBase}/block/${userB[1].id}`)
      .set("Authorization", `Bearer ${userC[2]}`)

    expect(blockConnection.status).to.equal(200)

    blockedUserConnectionId = blockConnection.body.id

    const userConnection = await UserConnection.findById(
      blockedUserConnectionId
    )

    expect(userConnection).to.have.a.property("_id")
    expect(userConnection.requester.toString()).to.equal(userC[1].id)
    expect(userConnection.recipient.toString()).to.equal(userB[1].id)
    expect(userConnection.status).to.equal(3)
  })

  // LIST ALL BLOCKED CONNECTIONS

  it("GET / It should list all blocked connections of userC", async () => {
    const userMyBlocked = await request(app)
      .get(`${urlBase}/blocked`)
      .set("Authorization", `Bearer ${userC[2]}`)

    expect(userMyBlocked.status).to.equal(200)
    expect(userMyBlocked.body).to.be.an("array").to.have.lengthOf(1)
    expect(userMyBlocked.body[0].username).to.equal("userB")
  })

  // DELETE/REJECT CONNECTIONS FROM A TO B

  it("DELETE / It should reject/remove the connection from the userA to userB", async () => {
    const rejectConnection = await request(app)
      .delete(`${urlBase}/reject/${userConnectionId}`)
      .set("Authorization", `Bearer ${userB[2]}`)

    expect(rejectConnection.status).to.equal(200)

    const userConnection = await UserConnection.findById(userConnectionId)

    expect(userConnection).to.equal(null)
  })

  // UNBLOCK B FROM C

  it("POST / It should unblock userB from userC", async () => {
    const rejectConnection = await request(app)
      .delete(`${urlBase}/reject/${blockedUserConnectionId}`)
      .set("Authorization", `Bearer ${userC[2]}`)

    expect(rejectConnection.status).to.equal(200)

    const userConnection = await UserConnection.findById(userConnectionId)

    expect(userConnection).to.equal(null)
  })

  after(async () => {
    // Remove all users
    const users = [userA, userB, userC]

    try {
      for (let i = 0; i < users.length; i++) {
        await request(app)
          .delete(`/api/v1/user/delete/${users[i][1].id}`)
          .set("Authorization", `Bearer ${users[i][2]}`)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
