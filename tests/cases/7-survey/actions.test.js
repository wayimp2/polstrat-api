import request from 'supertest'
import chai from 'chai'
import jwt from 'jsonwebtoken'
import app from '../../../src/app'
import { EMAIL_SECRET } from '../../../src/config'
import { Survey, User } from '../../../src/models'
import surveyDummy from '../../dummies/surveyDummy.json'
import userSurveyDummy from '../../dummies/dummyUsers/userSurveyDummy.json'

const expect = chai.expect
const urlBase = '/api/v1/survey'

describe('SURVEY ACTIONS', () => {
  let user = null
  let token = null
  let surveyId = null

  // CREATE DUMMY USER

  before(async () => {
    try {
      const userRes = await request(app)
        .post(`/api/v1/user/create`)
        .send(userSurveyDummy)

      const createdUser = await User.findById(userRes.body.id)
      user = createdUser

      const confirmToken = jwt.sign({ id: user.id }, EMAIL_SECRET, {
        expiresIn: '1d'
      })

      await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

      const AuthRes = await request(app)
        .post(`/api/v1/user/authenticate`)
        .send({
          username: userSurveyDummy.username,
          password: userSurveyDummy.password
        })

      token = AuthRes.body.token
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // CREATE SURVEY

  it('POST / It should create a survey', async () => {
    try {
      const res = await request(app)
        .post(`${urlBase}/create`)
        .send(surveyDummy)
        .set('Authorization', `Bearer ${token}`)

      expect(res.status).to.equal(200)

      surveyId = res.body.id
      const createdSurvey = await Survey.findById(surveyId)

      expect(createdSurvey).to.have.a.property('_id')
      expect(createdSurvey.owner._id.toString()).to.equal(user.id)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // UPDATE SURVEY

  it('PUT / It should update the survey', async () => {
    try {
      const newSurveyName = 'updatedSurvey'

      const res = await request(app)
        .put(`${urlBase}/update/${surveyId}`)
        .send({ name: newSurveyName })
        .set('Authorization', `Bearer ${token}`)

      expect(res.status).to.equal(200)

      const updatedSurvey = await Survey.findById(surveyId)

      expect(updatedSurvey.name).to.equal(newSurveyName)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // LIST SURVEY BY ID

  it('GET / It should list the survey by id', async () => {
    try {
      const resSurveyById = await request(app)
        .get(`${urlBase}/id/${surveyId}`)
        .set('Authorization', `Bearer ${token}`)

      expect(resSurveyById.status).to.equal(200)

      const surveyById = await Survey.findById(surveyId)

      expect(resSurveyById.body).to.have.a.property('_id')
      expect(resSurveyById.body._id.toString()).to.equal(
        surveyById._id.toString()
      )
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })


  // LIST ALL SURVEYS

  it('GET / It should list all the listed surveys', async () => {
    try {
      const resSurveyList = await request(app)
        .get(`${urlBase}/all`)
        .set('Authorization', `Bearer ${token}`)

      expect(resSurveyList.status).to.equal(200)

      expect(resSurveyList.body)
        .to.be.an('array')
        .to.have.lengthOf(1)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // DELETE SURVEY

  it('DELETE / It should delete the survey', async () => {
    try {
      const resDeletedSurvey = await request(app)
        .delete(`${urlBase}/delete/${surveyId}`)
        .set('Authorization', `Bearer ${token}`)

      expect(resDeletedSurvey.status).to.equal(200)

      const deletedSurvey = await Survey.findById(surveyId)
      const owner = await User.findById(user.id)

      expect(deletedSurvey).to.equal(null)
      expect(owner.Survey).to.equal(undefined)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // DELETE DUMMY USER
  after(async () => {
    try {
      await request(app)
        .delete(`/api/v1/user/delete/${user.id}`)
        .set('Authorization', `Bearer ${token}`)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
