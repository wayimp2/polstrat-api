import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { CommunityUser, User } from "../../../src/models"
import { EMAIL_SECRET } from "../../../src/config"
import userAdummy from "../../dummies/dummyUsers/userAdummy.json"
import userBdummy from "../../dummies/dummyUsers/userBdummy.json"
import userCdummy from "../../dummies/dummyUsers/userCdummy.json"
import communityDummy from "../../dummies/communityDummy.json"

const expect = chai.expect
const urlBase = "/api/v1/community/user"

describe("COMMUNITY USERS", () => {
  //ex. userAdummy => usersDummies[0][0] | user => usersDummies[0][1] | token => usersDummies[0][2]
  const usersDummies = [[userAdummy], [userBdummy], [userCdummy]]
  let communityId = null
  let communityUserId = null
  const userA = usersDummies[0]
  const userB = usersDummies[1]
  const userC = usersDummies[2]

  before(async () => {
    // Create users and authenticate them
    try {
      for (let i = 0; i < usersDummies.length; i++) {
        let userRes = await request(app)
          .post(`/api/v1/user/create`)
          .send(usersDummies[i][0])

        const createdUser = await User.findById(userRes.body.id)
        usersDummies[i].push(createdUser)

        const confirmToken = jwt.sign({ id: createdUser.id }, EMAIL_SECRET, {
          expiresIn: "1d",
        })

        await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

        const AuthRes = await request(app)
          .post(`/api/v1/user/authenticate`)
          .send({
            username: usersDummies[i][0].username,
            password: usersDummies[i][0].password,
          })

        usersDummies[i].push(AuthRes.body.token)
      }

      //userA creates a community
      const resCommunity = await request(app)
        .post(`/api/v1/community/create`)
        .send(communityDummy)
        .set("Authorization", `Bearer ${userA[2]}`)

      communityId = resCommunity.body.id
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("POST / It should request a owner of the community to join", async () => {
    try {
      const resRequestCommunity = await request(app)
        .get(`${urlBase}/request/${communityId}`)
        .set("Authorization", `Bearer ${userB[2]}`)

      expect(resRequestCommunity.status).to.equal(200)

      communityUserId = resRequestCommunity.body.id
      const requesterId = userB[1].id

      const communityUser = await CommunityUser.findById(communityUserId)

      expect(communityUser).to.have.a.property("_id")
      expect(communityUser.Community.toString()).to.equal(communityId)
      expect(communityUser.User.toString()).to.equal(requesterId)
      expect(communityUser.status).to.equal(1)
    } catch (e) {
      console.log(e)
    }
  })

  it("PUT / It should approve the user to join the community", async () => {
    const resApproveCommunity = await request(app)
      .put(`${urlBase}/approve/${communityUserId}`)
      .set("Authorization", `Bearer ${userA[2]}`)

    const communityUser = await CommunityUser.findById(communityUserId)

    expect(resApproveCommunity.status).to.equal(200)
    expect(communityUser.status).to.equal(2)
  })

  it("GET / It should list all the users from a community", async () => {
    const resListAll = await request(app)
      .get(`${urlBase}/all/${communityId}`)
      .set("Authorization", `Bearer ${userA[2]}`)

    expect(resListAll.status).to.equal(200)
    expect(resListAll.body).to.be.an("array").to.have.lengthOf(2)
  })

  it("POST / It should request of the community to join to be in pending state", async () => {
    try {
      const resRequestCommunity = await request(app)
        .get(`${urlBase}/request/${communityId}`)
        .set("Authorization", `Bearer ${userC[2]}`)

      expect(resRequestCommunity.status).to.equal(200)
    } catch (e) {
      console.log(e)
    }
  })

  it("GET / It should list all the pending users", async () => {
    const resListAll = await request(app)
      .get(`${urlBase}/pending`)
      .set("Authorization", `Bearer ${userA[2]}`)

    expect(resListAll.status).to.equal(200)
    expect(resListAll.body).to.be.an("array").to.have.lengthOf(1)
  })

  it("DELETE / It should reject/remove the request from the user to join community", async () => {
    const resCommunity = await request(app)
      .delete(`${urlBase}/reject/${communityUserId}`)
      .set("Authorization", `Bearer ${userA[2]}`)

    expect(resCommunity.status).to.equal(200)

    const communityUser = await CommunityUser.findById(communityUserId)

    expect(communityUser).to.equal(null)
  })

  after(async () => {
    // Remove owner and all users. Subsequently remove community
    const users = [userA, userB, userC]

    try {
      for (let i = 0; i < users.length; i++) {
        await request(app)
          .delete(`/api/v1/user/delete/${users[i][1].id}`)
          .set("Authorization", `Bearer ${users[i][2]}`)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
