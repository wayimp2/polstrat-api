import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { Message, User } from "../../../src/models"
import { EMAIL_SECRET } from "../../../src/config"
import messageDummy from "../../dummies/messageDummy.json"
import userAdummy from "../../dummies/dummyUsers/userAdummy.json"
import userBdummy from "../../dummies/dummyUsers/userBdummy.json"

const expect = chai.expect
const urlBase = "/api/v1/message"

describe("MESSAGES", () => {
  //ex. userAdummy => usersDummies[0][0] | user => usersDummies[0][1] | token => usersDummies[0][2]
  const usersDummies = [[userAdummy], [userBdummy]]
  const userA = usersDummies[0]
  const userB = usersDummies[1]
  let messageId = null

  before(async () => {
    // Create users and authenticate them
    try {
      for (let i = 0; i < usersDummies.length; i++) {
        let userRes = await request(app)
          .post(`/api/v1/user/create`)
          .send(usersDummies[i][0])

        const createdUser = await User.findById(userRes.body.id)
        usersDummies[i].push(createdUser)

        const confirmToken = jwt.sign({ id: createdUser.id }, EMAIL_SECRET, {
          expiresIn: "1d",
        })

        await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

        const AuthRes = await request(app)
          .post(`/api/v1/user/authenticate`)
          .send({
            username: usersDummies[i][0].username,
            password: usersDummies[i][0].password,
          })

        usersDummies[i].push(AuthRes.body.token)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  it("POST / It should send a message from userA to userB", async () => {
    try {
      const sendMessage = await request(app)
        .post(`${urlBase}/send/${userB[1].id}`)
        .send(messageDummy)
        .set("Authorization", `Bearer ${userA[2]}`)

      expect(sendMessage.status).to.equal(200)

      messageId = sendMessage.body.id

      const message = await Message.findById(messageId)

      expect(message).to.have.a.property("_id")
      expect(message.requester.toString()).to.equal(userA[1].id)
      expect(message.recipient.toString()).to.equal(userB[1].id)
      expect(message.state).to.equal(1)
    } catch (e) {
      console.log(e)
    }
  })

  it("POST / It should send a message from userB to userA", async () => {
    try {
      const sendMessage = await request(app)
        .post(`${urlBase}/send/${userA[1].id}`)
        .send({ text: "Text of userB to userA..." })
        .set("Authorization", `Bearer ${userB[2]}`)

      expect(sendMessage.status).to.equal(200)
    } catch (e) {
      console.log(e)
    }
  })

  it("POST / It should send another message from userA to userB", async () => {
    try {
      const sendMessage = await request(app)
        .post(`${urlBase}/send/${userB[1].id}`)
        .send({ text: "Text of another message of userA to userB..." })
        .set("Authorization", `Bearer ${userA[2]}`)

      expect(sendMessage.status).to.equal(200)
    } catch (e) {
      console.log(e)
    }
  })

  // RETRIEVE CONVERSATION

  it("GET / It should retrieve the conversation between userA and userB", async () => {
    try {
      const retrieveMessage = await request(app)
        .get(`${urlBase}/conversation/${userB[1].id}`)
        .set("Authorization", `Bearer ${userA[2]}`)

      expect(retrieveMessage.status).to.equal(200)

      const conversation = await Message.find({
        $or: [
          { requester: userA[1].id, recipient: userB[1].id },
          { requester: userB[1].id, recipient: userA[1].id },
        ],
      })

      expect(retrieveMessage.body).to.be.an("array").to.have.lengthOf(3)
      expect(conversation).to.be.an("array").to.have.lengthOf(3)
    } catch (e) {
      console.log(e)
    }
  })

  it("PUT / It should change the state of the message from unread to read", async () => {
    try {
      const readMessage = await request(app)
        .put(`${urlBase}/read/${messageId}`)
        .set("Authorization", `Bearer ${userB[2]}`)

      expect(readMessage.status).to.equal(200)

      const message = await Message.findById(messageId)

      expect(message.state).to.equal(2)
    } catch (e) {
      console.log(e)
    }
  })

  it("DELETE / It should delete the message from the userA to userB", async () => {
    try {
      const readMessage = await request(app)
        .delete(`${urlBase}/delete/${messageId}`)
        .set("Authorization", `Bearer ${userA[2]}`)

      expect(readMessage.status).to.equal(200)

      const message = await Message.findById(messageId)

      expect(message).to.equal(null)
    } catch (e) {
      console.log(e)
    }
  })

  after(async () => {
    // Remove all users
    const users = [userA, userB]

    try {
      for (let i = 0; i < users.length; i++) {
        await request(app)
          .delete(`/api/v1/user/delete/${users[i][1].id}`)
          .set("Authorization", `Bearer ${users[i][2]}`)
      }
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
