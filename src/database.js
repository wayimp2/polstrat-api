import mongoose from 'mongoose'
import { MONGO_CS, MONGOENV, MONGOPORT, MONGONAME } from './config'
import * as models from './models'

export default class Database {
  mongoose
  models = []
  #connection = MONGO_CS //`mongodb://${MONGOENV}:${MONGOPORT}/${MONGONAME}`
  #options = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }
  indexModels = ['User', 'Community']
  modelSchemas = models
  schemas = []

  async build () {
    try {
      this.mongoose = mongoose

      await this.#connect()
      await this.#buildSchemas()
      await this.#initMethods()
      await this.#loadModels()
      await this.#syncIndexes()

      return mongoose
    } catch (e) {
      console.log(e)
    }
  }

  #connect () {
    this.mongoose.connect(this.#connection, this.#options)
    console.log(`[DATABASE|CONNECTION]`)
    console.log(`[DATABASE]: Connected at ${this.#connection}`)
  }

  #buildSchemas () {
    console.log(`[DATABASE|MODELS|SCHEMAS]`)
    for (let modelName in this.modelSchemas) {
      const model = this.modelSchemas[modelName]
      console.log(`[DATABASE]: (${model.name}) configured.`)

      // build schemas
      const schema = new this.mongoose.Schema(
        model.schema(this.mongoose),
        model.options
      )
      this.schemas.push([modelName, schema])
    }
  }

  #initMethods () {
    console.log(`[DATABASE|SCHEMA|METHODS]`)

    for (let [name, schema] of this.schemas) {
      const model = this.modelSchemas[name]

      // initialize methods
      if (typeof model.methods !== 'undefined') {
        for (let method of model.methods(this.mongoose.models)) {
          // lets see what arguments the method needs
          if (
            typeof method.content !== 'undefined' ||
            typeof method.options !== 'undefined'
          ) {
            // if method is a indexing or similar type of fields
            schema[method.type](method.content || {}, method.options || {})
            console.log(
              `[DATABASE]: (${model.name}) - [${method.type}] Initiated.`
            )
          } else {
            // if method is a virtual field
            if (method.type === 'virtual') {
              if (method.set) {
                // if is a setter method
                schema[method.type](method.field).set(method.function)
              } else {
                // if is a getter method
                schema[method.type](method.field).get(method.function)
              }
            } else {
              // if method have name or identifier identifier
              if (method.option) {
                schema[method.type](
                  method.option,
                  method.function || method.value
                )
              } else {
                // if is a msimple method only calls function
                schema[method.type](method.function)
              }
            }

            // just logging...
            let methodName = method.name || method.field || method.option
            let isVirtual = method.type === 'virtual'
            console.log(
              `[DATABASE]: (${model.name}) Method - [${method.type}] ${
                methodName ? `[${methodName}]` : null
              } ${
                isVirtual ? `[${method.get ? 'get' : 'set'}]` : ''
              } Initiated.`
            )
          }
        }
      }
    }
  }

  #loadModels () {
    console.log(`[DATABASE|MODELS|BUILD]`)
    for (let [name, schema] of this.schemas) {
      // Set models
      this.models.push(new this.mongoose.model(name, schema))
      console.log(`[DATABASE]: (${name}) build.`)
    }
  }

  #syncIndexes () {
    console.log(`[DATABASE|MODELS|INDEXING]`)
    for (let models of this.indexModels) {
      this.mongoose.model(models).syncIndexes()
      console.log(`[DATABASE]: (${models}) Indexes synchronized.`)
    }
  }
}
