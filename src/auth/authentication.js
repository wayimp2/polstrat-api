import jwt from "jsonwebtoken"
import unless from "express-unless"
import validator from "validator"
import { ACCESS_SECRET } from "../config"

// AUTHENTICATE

const authentication = User => {
  const middleware = (req, res, next) => {
    const token = getToken(req)

    if (token === null) {
      return res.status(200).json({
        error: { access: true, empty: true },
      })
    }

    jwt.verify(token, ACCESS_SECRET, (err, payload) => {
      if (err) {
        return res.status(200).json({
          error: { access: true, expired: true },
        })
      }

      const { id } = payload

      if (!validator.isMongoId(id)) {
        return res.status(200).json({
          error: { access: true, id: true },
        })
      }

      const user = checkUserId(id, User)

      if (!user) {
        return res.status(200).json({
          error: { access: true, notfound: true },
        })
      }

      // Set user in request
      req.user = { id }

      req.payload = payload

      next()
    })
  }

  middleware.unless = unless

  return middleware
}

async function checkUserId(id, User) {
  try {
    return await User.findById(id, "_id")
  } catch (error) {
    return false
  }
}

function getToken(req) {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    return req.headers.authorization.split(" ")[1]
  } else if (req.query && req.query.token) {
    return req.query.token
  }
  return null
}

export default authentication
