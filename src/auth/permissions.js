// Get user permissions database fields
export default models => {
  const { User, UserConnection } = models

  // get the fields with a target user
  return async ({ user, params }) => {
    let fields = []
    const defaultFields = ["alias", "displayName"].join(" ")

    const userVisibility = await User.findById(params.id)

    // if the profile option is to private return default
    if (userVisibility.visibility.profile === 1) {
      return defaultFields
    }

    // lets get the state of the users connection
    const communityUser = await UserConnection.findOne({
      $or: [
        { requester: user.id, recipient: params.id },
        { requester: params.id, recipient: user.id },
      ],
    })

    // if its blocked return only username field
    if (communityUser && communityUser.status === 3) {
      return defaultFields
    }

    // lets see if the users are connected
    let isConnected = false
    if (communityUser && communityUser.status === 2) {
      isConnected = true
    }

    for (let key in userVisibility.visibility) {
      if (userVisibility.visibility.hasOwnProperty(key)) {
        // remove fields that dont belong on the model
        if (key === "profile" || key == "$init") {
          continue
        }

        // If profile field is only visible to connected users
        if (isConnected && userVisibility.visibility.profile === 2) {
          if (key === "email" || key === "ribbons") {
            fields.push(key)
          }

          fields.push(`information.${key}`)
        }

        // lets refine email and ribbons fields without pre "information."
        if (key === "email" || key === "ribbons") {
          if (userVisibility.visibility[key] === 3) {
            fields.push(key)
            continue
          }
        }

        // if they arent connected, get public fields
        if (userVisibility.visibility[key] === 3) {
          fields.push(`information.${key}`)
          continue
        }

        // If they are connected, get all friends fields
        if (isConnected && userVisibility.visibility[key] === 2) {
          fields.push(`information.${key}`)
        }
      }
    }

    // default fields if is empty
    fields.push(defaultFields)

    return fields.join(" ")
  }
}
