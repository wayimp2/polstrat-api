import nodemailer from "nodemailer"
import { EMAIL_SERVICE, EMAIL_USER, EMAIL_PASS } from "./config"
import { API_BASE_URL, FRONTEND_URL } from "./config"
const options = {
  service: EMAIL_SERVICE,
  auth: {
    user: EMAIL_USER,
    pass: EMAIL_PASS,
  },
}

export const transporter = ({ email, subject = "", html = "" }) => {
  try {
    if (!email) {
      throw new Error("Need email recipient to send mail")
    }

    const transporter = nodemailer.createTransport(options)

    transporter.sendMail({ to: email, subject, html })
  } catch (e) {
    console.log(e)
    throw new Error(e)
  }
}

export const confirmEmail = (user, emailToken) => {
  const link = `${FRONTEND_URL}/confirm-email/${emailToken}`

  console.log('link:' + link)

  const subject = "Polstrat Community Hub - Email Confirmation"

  const html = `
    <div style="text-align: center;">
      <h1>Polstrat Community Hub</h1>
      <h4>Hi ${user.displayName}, welcome to Community hub</h4>
      </br>
      <p>
        You've successfully created an account on the Polstrat Community Hub,</br>
        only need to click on the link below to confirm your account and start using the platform.
      </p>
      </br>
      <a target="_blank" href="${link}">CONFIRM ACCOUNT</a>
      </br>
      </br>
      </br>
      </br>
      </br>
      </br>
      <b style="text-align: center;">${getYear()} - <a href="http://polstrat.org">Polstrat</a> project</b>
      </br>
      <small style="text-align: center;">If you are receiving this email by mistake, just ignore.</small>
    </div>
    `

  transporter({ email: user.email, subject, html })
}

export const resetPasswordEmail = (email, resetPasswordToken) => {
  const link = `${API_BASE_URL}/api/auth/change-password/${resetPasswordToken}`

  const subject = "Polstrat Community Hub - Reset Password"

  const html = `
    <div style="text-align: center;">
      <h1>Polstrat Community Hub</h1>
      <h4>Reset password email</h4>
      </br>
      <p>
        You've requested to reset your password. Reset your password by clicking</br>
        on the link below where you will be redirected to the reset password page.</br>
      </p>
      </br>
      <a target="_blank" href="${link}">RESET PASSWORD</a>
      </br>
      </br>
      <p>
        If your account was locked, it will be unlocked after you change the password.</br>
        This link will expire in 24h from now.
      </p>
      </br>
      </br>
      <b style="text-align: center;">${getYear()} - <a href="http://polstrat.org">Polstrat</a> project</b>
      </br>
      <small style="text-align: center;">If you are receiving this email by mistake, just ignore.</small>
    </div>
    `

  transporter({ email, subject, html })
}

function getYear() {
  const today = new Date()
  return today.getFullYear()
}
