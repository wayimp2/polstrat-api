//TODO
export default class Endpoint {
  name
  options
  content

  constructor(name, options, content) {
    this.name = name
    this.options = options
    this.content = content
  }

  getOptions() {
    return { name: this.name, options: this.options }
  }

  getServices() {
    return { name: this.name, content: this.content }
  }
}
