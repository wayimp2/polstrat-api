import express from "express"
import nodepath from "path"

export default class Router {
  app
  mainRouter = express.Router()
  endpointsRouter = express.Router()

  acceptedMethods = ["GET", "POST", "PUT", "DELETE"]
  prefix = "/api"
  mainUrl

  parameterRoutes = []
  routeName = null

  async setRoutes(endpoints, app) {
    try {
      // set the main router with the options
      for (let i in endpoints) {
        const { name, options, content } = endpoints[i]
        this.name = name

        // set top level routes
        await this.#setMainRoute(options)

        // set the endpoints router with the content
        await this.#setSimpleRoute(content)

        // set routes with paramters at the end of the chain
        if (this.parameterRoutes.length && this.name === this.routeName) {
          await this.#setParameterRoute(this.parameterRoutes)
          this.parameterRoutes = []
          this.routeName = null
        }
      }

      // register th routes in th app
      await this.registerRoutes(app)
    } catch (e) {
      console.log(e)
    }
  }

  async registerRoutes(app) {
    try {
      await app.use(this.mainRouter)
      await app.use(this.endpointsRouter)
    } catch (e) {
      console.log(e)
    }
  }

  addMainRoute(url) {
    this.mainRouter.use(`${url}`, this.endpointsRouter)
    console.log(`[ROUTER]: - route: "${url}"`)
  }

  addMainMiddleware(url, fns) {
    this.mainRouter.use(url, fns)
  }

  addEndpointRoute(method, url, fns) {
    this.endpointsRouter[method.toLowerCase()](`${url}`, fns)
    console.log(`[ROUTER]:     - ${method} "${url}"`)
  }

  addEndpointMiddleware(url, fns) {
    this.endpointsRouter.use(url, fns)
  }

  #setMainRoute(options) {
    const { pre, post, url } = options
    console.log(`[ROUTER]: (${this.name.toUpperCase()})`)

    let setUrl
    if (url === "/") {
      this.mainUrl = setUrl = nodepath.join(this.prefix)
    } else {
      this.mainUrl = setUrl = nodepath.join(this.prefix, url)
    }

    // if we have a pre middleware
    if (Array.isArray(pre) || typeof pre === "function") {
      this.addMainMiddleware(setUrl, pre)
      console.log(`[ROUTER]: - (pre): middleware registed`)
    }

    // add top level route
    this.addMainRoute(setUrl)

    // if we have a pre middleware
    if (Array.isArray(post) || typeof post === "function") {
      this.addMainMiddleware(setUrl, post)
      console.log(`[ROUTER]: - (post): middleware registed`)
    }
  }

  // Set routes with paramters in the end of chain
  #setParameterRoute(endpoints) {
    for (let endpoint of endpoints) {
      this.#setEndpointsRoutes(endpoint.content, endpoint.name)
    }
  }

  // Set routes with checking if have parameters
  #setSimpleRoute(endpoints) {
    for (let i in endpoints) {
      // Lets see if is a paramter route, if yes, register after all routes
      const isParameter = /[:]+/

      if (isParameter.test(endpoints[i].path)) {
        this.routeName = this.name
        this.parameterRoutes.push({ name: i, content: endpoints[i] })
        continue
      }

      this.#setEndpointsRoutes(endpoints[i], i)
    }
  }

  #setEndpointsRoutes(endpoint, name) {
    const { path, handler, method, methods, pre, post } = endpoint

    console.log(`[ROUTER]:   (${name}):`)

    // Start to prepare to register routes
    let setUrl
    if (path === "/") {
      setUrl = ""
    } else {
      setUrl = nodepath.join(this.mainUrl, path)
    }
    // if we have a pre middleware
    if (Array.isArray(pre) || typeof pre === "function") {
      this.addEndpointMiddleware(setUrl, pre)
      console.log(`[ROUTER]:    - (pre): middleware registed`)
    }

    if (method && typeof method === "string") {
      let filteredMethod = method.toUpperCase()
      if (this.acceptedMethods.includes(filteredMethod)) {
        this.addEndpointRoute(filteredMethod, setUrl, handler)
      }
    }

    // if is an array of methods
    if (methods && Array.isArray(methods) && methods.length) {
      for (let method of methods) {
        let filteredMethod = method.toUpperCase()
        if (this.acceptedMethods.includes(filteredMethod)) {
          this.addEndpointRoute(filteredMethod, setUrl, handler)
        }
      }
    }

    // if we have a post middleware
    if (Array.isArray(post) || typeof post === "function") {
      this.addEndpointMiddleware(setUrl, post)
      console.log(`[ROUTER]:    - (post): middleware registed`)
    }
  }
}
