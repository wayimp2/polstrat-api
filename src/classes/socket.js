import socketServer from "socket.io"
import { FRONTEND_URL } from "../config"
import { isMongoId } from "../helpers/customValidators"

export default class Socket {
  io
  serverOptions = {
    transports: ["websocket", "polling", "flashsocket"],
    cors: {
      origin: FRONTEND_URL,
      methods: ["GET"],
      credentials: true,
    },
  }

  async init(server) {
    try {
      this.server = server

      await this.connectServer()
      await this.connection()
    } catch (e) {
      console.log(e)
    }
  }

  async connectServer() {
    try {
      console.log(`[SOCKET]: Connected to server`)
      this.io = await socketServer(this.server, this.serverOptions)
    } catch (e) {
      console.log(e)
    }
  }

  connection() {
    console.log(`[SOCKET]: Receiving connections established...`)
    this.io.on("connection", socket => this.listeners(socket), {
      "sync disconnect on unload": true,
    })
  }

  listeners(socket) {
    // SET USER AT CONNECT
    socket.on("USER_CONNECT", id =>
      socket.on(id, name => this.setUserConnection(socket, id, name))
    )

    // DISCONNECT
    socket.on("disconnect", () => this.disconnect(socket))
  }

  // METHODS

  emitToUser(type, id, data) {
    this.io.in(id).emit(type, data)
  }

  //ON CONNECTION

  // On user connect
  setUserConnection(socket, id, name) {
    socket.join(id)
    socket.user = {
      id,
      name,
    }

    const client = this.io.sockets.adapter.rooms.get(id)

    if (client.size <= 1 || id === "unknown") {
      this.log("connected", "online", socket)
      return
    }

    this.log("client", "client connected", socket)
  }

  //ON DISCONNECT

  disconnect(socket) {
    if (!socket.user) {
      return
    }

    const client = this.io.sockets.adapter.rooms.get(socket.user.id)

    if (!client || socket.user.id === "unknown") {
      this.log("disconnected", "offline", socket)
      return
    }

    this.log("client", "client disconnected", socket)
  }

  log(title, type, socket) {
    const time = `\n- TIME: ${new Date().toLocaleString()}`
    const name = socket.user.name
    const id = socket.user.id

    const client = this.io.sockets.adapter.rooms.get(id)

    let state =
      type === "online" || type === "offline"
        ? type.toUpperCase()
        : `${type}: ${client ? client.size : null}`

    let ua =
      type === "online" ? `\n- UA: ${socket.request.headers["user-agent"]}` : ""

    let ip =
      type === "online"
        ? `\n- IP: ${socket.request.connection.remoteAddress}`
        : ""

    let socketId = `\n- ID: ${socket.id}`

    console.log(
      `[SOCKET][${title.toUpperCase()}]: [${name}] - (${state})${ip}${ua}${socketId}${time}\n`
    )
  }
}
