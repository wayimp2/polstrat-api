import dotenv from 'dotenv'
dotenv.config()

const defaults = {
  port: 9091,
  host: '0.0.0.0',
  protocol: 'http://',

  mongoPort: '27017',
  mongoName: 'pscommunityhub',

  frontendPort: 3000
}

// ENVIRONMENT

const environment = process.env.NODE_ENV || 'dev'

export const ENV = environment == 'dev' ? 'development' : 'production'
export const DEV = environment == 'dev'
export const PROD = environment == 'prod'

// SERVER

export const PROTOCOL = process.env.PROTOCOL || defaults.protocol
export const HOST = process.env.HOST || defaults.host
export const PORT = process.env.PORT || defaults.port

export const API_BASE_URL =
  process.env.API_BASE_URL || [PROTOCOL, HOST, ':', PORT].join('')

// FRONTEND

const defaultFrontendUrl = `${defaults.protocol}${defaults.host}:${defaults.frontendPort}`

export const FRONTEND_URL = 'https://ps.boquetemarket.com' //process.env.FRONTEND_URL || defaultFrontendUrl

// CORS
export const CORS_WHITELIST = [FRONTEND_URL]

// MONGO

export const MONGO_CS = process.env.MONGO_CS
export const MONGOENV = environment == 'dev' ? defaults.host : 'mongo'
export const MONGOPORT = process.env.MONGO_PORT || defaults.mongoPort
export const MONGONAME = process.env.MONGO_NAME || defaults.mongoName

// EMAIL

export const EMAIL_SERVICE = 'gmail' // process.env.EMAIL_SERVICE || 
export const EMAIL_USER = 'boquetemarket@gmail.com' // process.env.EMAIL_USER || 
export const EMAIL_PASS = 'java6550' // process.env.EMAIL_PASSWORD || 

// JWT
export const ACCESS_SECRET =
  process.env.ACCESS_SECRET || 'the-lazy-dog-jumps-over-the-quick-brown-fox'
export const REFRESH_SECRET =
  process.env.REFRESH_SECRET || 'the-lazy-dog-jumps-over-the-quick-brown-fox'
export const EMAIL_SECRET =
  process.env.EMAIL_SECRET || 'the-lazy-dog-jumps-over-the-quick-brown-fox'
export const RESET_PASSWORD_SECRET =
  process.env.RESET_PASSWORD_SECRET ||
  'the-lazy-dog-jumps-over-the-quick-brown-fox'
