import express from "express"
import fs from "fs"
import { ENV, API_BASE_URL, PORT, FRONTEND_URL } from "./config"
import server from "./server"
import Socket from "./classes/socket"
import Database from "./database"
import { middlewares, errorHandlerMiddleware } from "./middlewares"
import middlewareFilters from "./auth/middlewareFilters"
import authentication from "./auth/authentication"
import authorized from "./auth/authorized"
import permissions from "./auth/permissions"
import Router from "./classes/router"
import * as adapters from "./adapters"
import * as endpoints from "./endpoints"
import * as services from "./services"

export default class App {
  port = PORT
  env = ENV
  base_url = API_BASE_URL

  app = express()
  server
  socket
  db
  adapters = {}
  endpoints = []

  async start() {
    try {
      console.log(`\n[POLSTRAT | COMMUNITY HUB] (API)\n`)
      console.log(`[ENVIRONMENT]: ${this.env}`)
      await this.initServer()
      await this.#initDatabase()
      await this.initSocket()
      await this.initMiddlewares()
      await this.#initAuthentication()
      await this.initAdapters()
      await this.initEndpoints()
      await this.initRouter()
      await this.initErrorsHandlers()
      console.log(`[FINISH]: Finishing...`)
      console.log(`\n[READY]: App ready at: ${this.base_url}\n`)
      console.log(`\n[SOCKETS|LISTENING]: Waiting for socket connections...`)
    } catch (e) {
      console.log(e)
    }
  }

  async initServer() {
    try {
      await server.apply(this)
    } catch (e) {
      console.log(e)
    }
  }

  async #initDatabase() {
    try {
      const database = new Database()
      this.db = await database.build()
    } catch (e) {
      console.log(e)
    }
  }

  async initSocket() {
    try {
      console.log(`[SOCKET|CONNECTED]`)
      const socket = new Socket()
      await socket.init(this.server)

      this.socket = socket
    } catch (e) {
      console.log(e)
    }
  }

  initMiddlewares() {
    console.log(`[MIDDLEWARES|STARTING]`)

    for (let [name, middleware] of middlewares) {
      this.app.use(middleware)
      console.log(`[MIDDLEWARES]: "${name}" applied.`)
    }
  }

  #initAuthentication() {
    console.log(`[AUTHENTICATION|STARTING]`)

    this.app.use(authentication(this.db.models["User"]).unless(authorized))
    console.log(
      `[AUTHENTICATION]: Authorized token/tokenless routes initialized.`
    )
  }

  initAdapters() {
    console.log(`[ADAPTERS|SETUP]`)
    for (let name in adapters) {
      console.log(`[ADAPTERS]: (${name.toUpperCase()}) initialized...`)

      this.adapters[name] = adapters[name]({
        models: this.db.models,
        permissions: permissions(this.db.models),
        socket: this.socket,
      })
    }
  }

  async initEndpoints() {
    try {
      console.log(`[ENDPOINTS|SETUP]`)
      for (let name in endpoints) {
        console.log(`[ENDPOINTS]: (${name.toUpperCase()}) initiated...`)

        if (!services[name]) {
          await this.endpoints.push(
            endpoints[name](middlewareFilters(this.db.models))
          )
          console.log(`[ENDPOINTS]: - No endpoint applied.`)
        } else {
          await this.endpoints.push(
            endpoints[name](
              // endpoints args..
              services[name]({
                models: this.db.models,
                permissions: permissions(this.db.models),
                socket: this.socket,
                adapters: this.adapters,
              }),
              middlewareFilters(this.db.models)
            )
          )
          console.log(
            `[ENDPOINTS]: - Service "${name}" was applied to this endpoint.`
          )
        }
      }
    } catch (e) {
      console.log(e)
    }
  }

  async initRouter() {
    try {
      console.log(`[ROUTER|INITIALIZED]`)
      const router = new Router()
      await router.setRoutes(this.endpoints, this.app)
    } catch (e) {
      console.log(e)
    }
  }

  initErrorsHandlers() {
    console.log(`[ERROR-HANDLER|STARTING]`)
    for (let [name, middleware] of errorHandlerMiddleware) {
      this.app.use(middleware)
      console.log(`[ERROR HANDLER]: ${name} inititalized.`)
    }
  }
}
