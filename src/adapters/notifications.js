import Adapter from "../classes/adapter"

const notification = ({ models }) => {
  const { Notification } = models

  return new Adapter(
    // Name
    "notification",

    // Methods
    {
      // send a message to a user
      create: async ({ userId, model, subject }) => {
        if (!model || !model.name || !model.id) {
          throw new Error("Model specification not correct.")
        }

        await Notification.findOneAndUpdate(
          { User: userId, [model.name]: model.id, subject: subject },
          { $set: { state: 1 } },
          { upsert: true, new: true }
        )
      },

      // send a message to a user
      seen: async id => {
        if (!id) {
          throw new Error("Id specification not correct.")
        }

        await Notification.findOneAndUpdate({ _id: id }, { $set: { state: 2 } })
      },

      // send a message to a user
      getAllFromUser: async userId => {
        if (!userId) {
          throw new Error("User id not specified.")
        }

        return await Notification.find({
          User: userId,
        })
          .sort({ date: "desc" })
          .populate({
            path: "UserConnection",
            populate: { path: "requester", select: "displayName" },
          })
          .populate({
            path: "Message",
          })
          .populate({
            path: "Community",
            select: "displayName",
          })
      },

      // send a message to a user
      getAllFromUserCountNew: async userId => {
        if (!userId) {
          throw new Error("User id not specified.")
        }

        return await Notification.countDocuments({
          User: userId,
          state: 1,
        })
      },

      // Add more here...
    }
  )
}

export default notification
