import Service from '../classes/service'

const survey = ({ models }) => {
  const { Survey } = models
  const { Device } = models

  return new Service(
    // Name
    'survey',

    // Methods
    {
      create: async params => {
        const survey = new Survey(params)

        // Validate before save, if one fails, the other fails too
        await survey.validate()
        await survey.save()

        return survey
      },

      update: async (id, params) => {
        await Survey.findOneAndUpdate({ _id: id }, params)
      },

      delete: async id => await Survey.findByIdAndDelete(id),

      all: async (limit = 25) => {
        return await Survey.find({}).limit(+limit)
      }
    }
  )
}

export default survey
