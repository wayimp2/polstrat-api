import Service from "../classes/service"
import { portraitFields } from "../auth/defaultFields"

const userConnection = ({ models, permissions, socket, adapters }) => {
  const { UserConnection } = models

  return new Service(
    // Name
    "user-connection",

    // Methods
    {
      request: async (requester, recipient) => {
        const connection = await UserConnection.findOneAndUpdate(
          { requester: requester, recipient: recipient },
          { $set: { status: 1 } },
          { upsert: true, new: true }
        ).populate({
          path: "requester",
          select: "displayName",
        })

        await UserConnection.findOneAndUpdate(
          { requester: recipient, recipient: requester },
          { $set: { status: 0 } },
          { upsert: true, new: true }
        )

        await adapters.notification.create({
          userId: recipient,
          subject: "request",
          model: {
            id: connection._id,
            name: "UserConnection",
          },
        })

        socket.emitToUser("FRIEND_REQUEST", recipient, {
          id: connection.requester._id,
          name: connection.requester.displayName,
        })
      },

      approve: async (requester, recipient) => {
        await UserConnection.findOneAndUpdate(
          { requester: requester, recipient: recipient },
          { $set: { status: 2 } }
        )
        await UserConnection.findOneAndUpdate(
          { requester: recipient, recipient: requester },
          { $set: { status: 2 } }
        )

        socket.emitToUser("FRIEND_APPROVED", recipient, requester)
      },

      // remove or reject connection
      reject: async (requester, recipient) => {
        // need to delete one by one to trigger hook to delete notification
        await UserConnection.findOneAndDelete({
          requester: requester,
          recipient: recipient,
        })
        await UserConnection.findOneAndDelete({
          requester: recipient,
          recipient: requester,
        })

        socket.emitToUser("FRIEND_REJECTED", recipient, requester)
      },

      // block other user
      block: async (requester, recipient) => {
        const requesterConnection = await UserConnection.findOneAndUpdate(
          { requester: requester, recipient: recipient },
          { $set: { status: 3 } },
          { upsert: true, new: true }
        )

        await UserConnection.findOneAndUpdate(
          { requester: recipient, recipient: requester },
          { $set: { status: 0 } },
          { upsert: true, new: true }
        )

        socket.emitToUser("BLOCKED", recipient, requester)

        return requesterConnection
      },

      // GET INFO

      // get my all connections
      my: async (requesterId, query) => {
        const { date, lastid, limit } = query

        const sort = {
          _id: -1,
        }

        let queryOptions = {}
        queryOptions.requester = requesterId
        queryOptions.status = 2

        const fields = "_id updatedAt"
        const populate = {
          path: "recipient",
          select: `${portraitFields} Community`,
          populate: {
            path: "Community",
            select: "tag alias",
          },
        }

        if (lastid) {
          queryOptions._id = { $lt: lastid }
        }

        const connections = await UserConnection.find(queryOptions, fields)
          .populate(populate)
          .sort(sort)
          .limit(+limit)

        return connections
      },

      // get my all connections
      countMy: async requesterId =>
        await UserConnection.countDocuments({
          requester: requesterId,
          status: 2,
        }),

      // All requests made to me
      requests: async (requesterId, query) => {
        const { date, lastid, limit } = query

        const sort = {
          _id: -1,
        }

        let queryOptions = {}
        queryOptions.recipient = requesterId
        queryOptions.status = 1

        const fields = "_id createdAt"
        const populate = {
          path: "requester",
          select: `${portraitFields} Community`,
          populate: {
            path: "Community",
            select: "tag alias",
          },
        }

        if (lastid) {
          queryOptions._id = { $lt: lastid }
        }

        const connections = await UserConnection.find(queryOptions, fields)
          .populate(populate)
          .sort(sort)
          .limit(+limit)

        return connections
      },

      // get the count all requests
      countRequests: async requesterId =>
        await UserConnection.countDocuments({
          recipient: requesterId,
          status: 1,
        }),

      // All requestsive  made to anotehr user
      pending: async (requesterId, query) => {
        const { date, lastid, limit } = query

        const sort = {
          _id: -1,
        }

        let queryOptions = {}
        queryOptions.requester = requesterId
        queryOptions.status = 1

        const fields = "_id createdAt"
        const populate = {
          path: "recipient",
          select: `${portraitFields} Community`,
          populate: {
            path: "Community",
            select: "tag alias",
          },
        }

        if (lastid) {
          queryOptions._id = { $lt: lastid }
        }

        const connections = await UserConnection.find(queryOptions, fields)
          .populate(populate)
          .sort(sort)
          .limit(+limit)

        return connections
      },

      // get all users ive blocked
      blocked: async requesterId => {
        const { date, lastid, limit } = query

        const sort = {
          _id: -1,
        }

        let queryOptions = {}
        queryOptions.requester = requesterId
        queryOptions.status = 3

        const fields = "_id createdAt"
        const populate = {
          path: "recipient",
          select: `${portraitFields} Community`,
          populate: {
            path: "Community",
            select: "tag alias",
          },
        }

        if (lastid) {
          queryOptions._id = { $lt: lastid }
        }

        const connections = await UserConnection.find(queryOptions, fields)
          .populate(populate)
          .sort(sort)
          .limit(+limit)

        return connections
      },

      // Get connection between two users
      getConnection: async (requesterId, recipientId) => {
        const connections = await UserConnection.find(
          {
            $or: [
              {
                requester: requesterId,
                recipient: recipientId,
              },
              {
                recipient: requesterId,
                requester: recipientId,
              },
            ],
          },
          "status requester recipient"
        )

        let response = {}

        for (let connection of connections) {
          if (connection.requester.toString() === requesterId) {
            response.requester = connection
          } else {
            response.recipient = connection
          }
        }

        return response
      },

      // Get connection between two users
      getNotifications: async userId => {
        if (!userId) {
          throw new Error("User id not specified.")
        }

        return await adapters.notification.getAllFromUser(userId)
      },

      // Get connection between two users
      getNewNotifications: async userId => {
        if (!userId) {
          throw new Error("User id not specified.")
        }

        return await adapters.notification.getAllFromUserCountNew(userId)
      },

      // Get connection between two users
      notificationSeen: async id => {
        if (!id) {
          throw new Error("Notification id not specified.")
        }

        return await adapters.notification.seen(id)
      },

      // add more here...
    }
  )
}

export default userConnection
