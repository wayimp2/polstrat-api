import Service from "../classes/service"
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { customValidators } from "../helpers"
import { EMAIL_SECRET } from "../config"
import { confirmEmail } from "../nodemailer"
import { confirmEmailDuration } from "../auth/generate"

const user = ({ models, permissions }) => {
  const { User, Device } = models

  return new Service(
    // Name
    "user",

    // Methods
    {
      current: async id => {
        try {
          const fields = [
            "alias",
            "displayName",
            "email",
            "information",
            "questions",
            "ribbons",
            "visibility",
          ].join(" ")

          return await User.findOne({ _id: id }, fields).populate({
            path: "Community",
          })
        } catch (e) {
          next(e)
        }
      },

      getDevices: async req => await Device.find({ User: req.user.id }),

      create: async userParam => {
        // TODO: check already exist in one query for perfomance..

        // TODO: do this in the model instead of the service
        userParam.displayName = userParam.username.replace(/\s/g, "").trim()
        userParam.alias = userParam.username
          .toLowerCase()
          .replace(/\s/g, "")
          .trim()
        userParam.email = userParam.email.toLowerCase().trim()
        delete userParam.username

        // Check if username already exists
        if (await User.findOne({ alias: userParam.alias })) {
          throw new Error(
            `Username (${userParam.displayName}) is already taken`
          )
        }

        // Check if email already exists
        if (await User.findOne({ email: userParam.email })) {
          throw new Error(
            `Email (${userParam.email}) already exists on our database`
          )
        }

        if (customValidators.userPassword(userParam.password)) {
          // everything is ok lets start the user instantiation
          const user = new User(userParam)
          const saltRounds = 10

          // hash the password
          if (userParam.password) {
            user.hash = bcrypt.hashSync(userParam.password, saltRounds)
          }

          // see if saving will not have errors before sending email
          await user.validate()

          // Send asynchronous email, so no async
          jwt.sign(
            { id: user.id },
            EMAIL_SECRET,
            { expiresIn: confirmEmailDuration },
            (err, emailToken) => {
              if (err) {
                console.log(err)
                throw new Error(err)
              }

              confirmEmail(user, emailToken)
            }
          )

          await user.save()

          return user
        }
      },

      update: async (req, userParam) => {
        const user = await User.findById(req.user.id)

        if (!user) {
          throw new Error("User not found")
        }

        // if user its not admin or isnt the same user
        if (!user.admin && req.user.id !== req.params.id) {
          throw new Error("No permission to perform this action!")
        }

        // if user try to update admin role
        if (!user.admin && userParam.admin) {
          throw new Error("No permission to perform this action!")
        }

        if (userParam.password || userParam.admin || userParam.confirm) {
          throw new Error("No permission to perform this action")
        }

        if (userParam.username) {
          // TODO: do this in the model instead of the service
          userParam.displayName = userParam.username.replace(/\s/g, "").trim()
          userParam.alias = userParam.username
            .toLowerCase()
            .replace(/\s/g, "")
            .trim()
          userParam.email = userParam.email.toLowerCase().trim()
          delete userParam.username

          // check if the username doesnt exist, let pass the same username
          if (
            user.alias !== userParam.alias &&
            (await User.findOne({ alias: userParam.alias }))
          ) {
            throw new Error(
              `Username (${userParam.displayName}) is already taken`
            )
          }
        }

        // check if the email doesnt exist, let pass the same email
        if (
          user.email !== userParam.email &&
          (await User.findOne({ email: userParam.email }))
        ) {
          throw new Error(
            `Email (${userParam.email}) is already on our database`
          )
        }

        // merge differences
        await Object.assign(user, userParam)

        await user.save()

        return user
      },

      // delete user
      delete: async id => await User.findByIdAndDelete(id),

      // (Temporary)
      all: async req => await User.find({}, "-hash -admin -confirm"),

      // get user by alias
      getByAlias: async req => {
        const { alias } = req.params

        const user = await User.findOne({ alias: alias }, "_id")

        if (!user) {
          return false
        }

        req.params.id = user.id

        const fields = await permissions(req)

        return await User.findOne({ alias: alias }, fields).populate({
          path: "Community",
        })
      },

      // add more here...
    }
  )
}

export default user
