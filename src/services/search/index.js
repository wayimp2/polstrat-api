import Service from "../../classes/service"
import { queryInformation, queryRibbons } from "./queries/filter"
import { userTextQuery, communityTextQuery } from "./queries/text"
import {
  userSearchPortraitFields,
  communityPortraitFields,
} from "../../auth/defaultFields"

const search = ({ models }) => {
  const { User, Community } = models

  return new Service(
    // Name
    "search",

    // Methods
    {
      // search by text string
      text: async queryParams => {
        const { search } = queryParams
        const searchFilter = new RegExp(".*" + search + ".*", "i")

        // Users query
        const userQuery = userTextQuery(search, searchFilter)
        const userResult = await User.find(
          userQuery,
          userSearchPortraitFields
        ).populate("Community")

        // Communities query
        const communityQuery = communityTextQuery(search, searchFilter)
        const communityResult = await Community.find(
          communityQuery,
          communityPortraitFields
        )

        return {
          users: [...userResult],
          communities: [...communityResult],
        }
      },

      // search user by choosen filters
      userFilter: async queryParams => {
        let query = {}

        // Lets only pick the visible profiles
        query["visibility.profile"] = 2

        queryInformation(query, queryParams)
        queryRibbons(query, queryParams)

        return await User.find(query, userSearchPortraitFields)
      },

      // search community by choosen filters
      communityFilter: async queryParams => {
        let query = {}

        // Lets only pick the visible profiles
        query["listed"] = true

        for (let param in queryParams) {
          if (param) {
            query[param] = queryParams[param]
          }
        }

        return await Community.find(query, communityPortraitFields)
      },

      // Add more here...
    }
  )
}

export default search
