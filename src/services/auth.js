import Service from "../classes/service"
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { customValidators } from "../helpers"
import { REFRESH_SECRET, EMAIL_SECRET, RESET_PASSWORD_SECRET } from "../config"
import { confirmEmail, resetPasswordEmail } from "../nodemailer"
import {
  generateAccessToken,
  generateRefreshToken,
  confirmEmailDuration,
  restPasswordDuration,
} from "../auth/generate"

const RegExpEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const auth = ({ models }) => {
  const { User, Device } = models

  return new Service(
    // Name
    "auth",

    // Methods
    {
      login: async ({ username, password, remember = false, deviceInfo }) => {
        let credential = null

        // lets see if is logging with username or email
        if (RegExpEmail.test(String(username).toLowerCase())) {
          credential = { email: username.toLowerCase().trim() }
        } else {
          credential = {
            alias: username.toLowerCase().replace(/\s/g, "").trim(),
          }
        }

        let error = {
          username: false,
          password: false,
          confirmed: false,
          blocked: false,
          attempts: 0,
        }

        const user = await User.findOne(credential)

        if (!user) {
          error.username = true

          return { error }
        }

        // email verification
        /*if (!user.confirmed) {
          error.confirmed = true
          return { error }
        }
*/
        const now = new Date()

        // lets see if its locked from the attempts
        if (now < user.passwordLock) {
          error.blocked = true

          return { error }
        }

        // check and sign the token
        if (bcrypt.compareSync(password, user.hash)) {
          // sign token after confirm credentials

          const access = generateAccessToken(user.id)
          const refresh = generateRefreshToken(user.id, remember)

          // if succed the login reset the attempts
          user.passwordLock = undefined
          user.passwordAttempts = 0
          user.lastLogin = new Date()

          // lets check if user is fine before saving device
          await user.validate()

          // if want to remember browser, save device and token
          let device

          if (deviceInfo.ua) {
            device = await Device.findOne({
              User: user,
              ua: deviceInfo.ua,
            })
          }

          // If the device exist, update token only
          if (device) {
            device.token = refresh
          } else {
            // If not, lets create the device
            device = new Device(deviceInfo)

            device.User = user
            device.token = refresh
          }

          await device.validate()
          await device.save()

          await user.save()

          return { access, refresh }
        } else {
          // Password is wrong, increment the attempt
          user.passwordAttempts = user.passwordAttempts + 1
          error.attempts = user.passwordAttempts

          if (user.passwordAttempts >= 3) {
            const date = new Date()
            const timeLocked = 6 // 6 hours

            // If 3 attempts were made lets lock it
            user.passwordLock = customValidators.addHoursToDate(
              date,
              timeLocked
            )
            error.blocked = true
          }

          user.save()

          error.password = true
        }

        return { error }
      },

      logout: async (token, id) => {
        await Device.deleteOne({
          token: token,
        })
      },

      verifyRefreshToken: async token => {
        let error = {}

        // need async for this...
        const response = await jwt.verify(
          token,
          REFRESH_SECRET,
          async (err, payload) => {
            if (err) {
              error.refresh = true
              error.expired = true

              await Device.deleteOne({ token: token })

              return { error }
            }

            return payload
          }
        )

        if (response.error) {
          return response
        }

        const device = await Device.findOne({ token: token })

        if (!device) {
          error.refresh = true
          error.device = true
          return error
        }

        return { access: generateAccessToken(response.id) }
      },

      // Email with token to confirm account
      confirmEmailToken: user => {
        jwt.sign(
          { id: user.id },
          EMAIL_SECRET,
          { expiresIn: confirmEmailDuration },
          (err, emailToken) => {
            if (err) {
              console.log(err)
              throw new Error(err)
            }

            confirmEmail(user, emailToken)
          }
        )
      },

      // Email with token to reset password
      resetPassword: email => {
        // Send asynchronous email, so no async
        jwt.sign(
          { email: email },
          RESET_PASSWORD_SECRET,
          {
            expiresIn: restPasswordDuration,
          },
          (err, resetPasswordToken) => {
            if (err) {
              console.log(err)
              throw new Error(err)
            }

            resetPasswordEmail(email, resetPasswordToken)
          }
        )
      },

      // Change the password and delete sessions after reseting password
      changePassword: async (email, resetPassParam, reset = false) => {
        // Confirm passwords
        if (resetPassParam.password !== resetPassParam.confirmPassword) {
          throw new Error(`Confirm password don't match with password`)
        }

        const user = await User.findOne({ email: email })

        if (!user) {
          throw new Error("User not found")
        }

        // lets check if password have requirements
        if (
          resetPassParam.password &&
          customValidators.userPassword(resetPassParam.password)
        ) {
          resetPassParam.hash = bcrypt.hashSync(resetPassParam.password, 10)
        }

        //if the password is locked, unlock it.
        user.passwordLock = undefined
        user.passwordAttempts = 0

        // merge differences
        await Object.assign(user, resetPassParam)

        if (reset) {
          await Device.deleteMany({
            User: user,
          })
        }

        await user.save()
      },

      // Add more here...
    }
  )
}

export default auth
