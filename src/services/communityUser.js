import Service from "../classes/service"

const communityUser = ({ models }) => {
  const { CommunityUser, Community } = models

  return new Service(
    // Name
    "community-user",

    // Methods
    {
      // request to join community
      request: async (requester, communityId) => {
        const communityUser = await CommunityUser.findOne({ User: requester })

        // if user have other community requests, delete it
        if (communityUser && communityUser.status === 1) {
          await CommunityUser.findOneAndRemove({ _id: communityUser.id })
        }

        // Set the requester to requested state and create if didnt had one
        const communityUserRequest = await CommunityUser.findOneAndUpdate(
          { Community: communityId, User: requester },
          { $set: { Community: communityId, User: requester, status: 1 } },
          { upsert: true, new: true }
        )

        return communityUserRequest
      },

      // approve a user to the community
      approve: async id =>
        await CommunityUser.findOneAndUpdate(
          { _id: id },
          { $set: { status: 2 } }
        ),

      // reject or remove the request to the community owner
      reject: async id => await CommunityUser.findOneAndRemove({ _id: id }),

      // get all users that belongs to a community
      all: async communityId => {
        let users = []
        // Set the requester to requested state
        const communityUsers = await CommunityUser.find(
          {
            Community: communityId,
            status: 2,
          },
          "User -_id"
        ).populate({
          path: "User",
          select: userCommunityPortraitFields,
        })

        communityUsers.forEach(user => {
          users.push(user.User)
        })

        return users
      },

      // get a number with the number of members of a community
      count: async alias => {
        const community = await Community.findOne({ alias: alias })

        if (!community) {
          throw new Error("Community doesn't exist with that alias.")
        }

        // Set the requester to requested state
        const communityUsers = await CommunityUser.countDocuments({
          Community: community,
          status: 2,
        })

        return communityUsers
      },

      // get all requests i made to communities
      pending: async userId => {
        const community = await Community.findOne({
          owner: userId,
        })

        if (!community) {
          throw new Error("No community found with this owner")
        }

        let pendingUsers = []
        // Set the requester to requested state
        const communityUsers = await CommunityUser.find(
          {
            Community: community.id,
            status: 1,
          },
          "User -_id"
        ).populate({
          path: "User",
          select: userCommunityPortraitFields,
        })

        communityUsers.forEach(user => {
          pendingUsers.push(user.User)
        })

        return pendingUsers
      },

      // Add more here...
    }
  )
}

export default communityUser
