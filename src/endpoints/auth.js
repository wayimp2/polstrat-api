import Endpoint from "../classes/endpoint"
import jwt from "jsonwebtoken"
import { EMAIL_SECRET, RESET_PASSWORD_SECRET, FRONTEND_URL } from "../config"

const auth = endpoint => {
  return new Endpoint(
    // NAME
    "auth",

    // ENDPOINT URL
    { url: "/auth" },

    // ROUTES
    {
      // authenticate session
      login: {
        path: "/login",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const response = await endpoint.login(req.body)

            if (response.error) {
              res.status(200).json({
                status: true,
                options: response.error,
              })
            } else {
              res.status(200).json(response)
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // end session
      logout: {
        path: "/logout",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const token = req.body.token

            if (!token) {
              res.status(401).json("No token was found.")
              return
            }

            await endpoint.logout(token, req.user.id)

            res.status(200)
          } catch (e) {
            next(e)
          }
        },
      },

      // refresh jwt token
      refreshToken: {
        path: "/refresh",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const token = req.body.token

            if (!token) {
              res.status(200).json(false)
              return
            }

            const response = await endpoint.verifyRefreshToken(token)

            res.status(200).json(response)
          } catch (error) {
            console.log(error)
            throw new Error(error)
          }
        },
      },

      // send email with token to confirm the account
      confirmEmail: {
        path: "/confirm-email/:token",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const { id } = jwt.verify(req.params.token, EMAIL_SECRET)

            if (!id) {
              throw new Error("Need a valid id to confirm account")
            }

            const response = await endpoint.confirmEmail(id)

            res.status(200).json(response)
          } catch (e) {
            throw new Error(e)
          }
        },
      },

      // send email to reset password
      resetPassword: {
        path: "/reset-password",
        method: "POST",
        handler: async (req, res, next) => {
          //asynchrounly request, no async
          try {
            endpoint.resetPassword(req.body.email)

            res
              .status(200)
              .json({ message: "Reset password email was sent successfully" })
          } catch (e) {
            throw new Error(e)
          }
        },
      },

      // change password after reseting
      changePassword: {
        path: "/change-password/:token",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const { email } = jwt.verify(
              req.params.token,
              RESET_PASSWORD_SECRET
            )

            if (!email) {
              throw new Error("Need a valid email to reset password")
            }

            await endpoint.changePassword(email, req.body)

            // TODO: better request
            res.writeHead(302, {
              Location: FRONTEND_URL + "/",
              //add other headers here...
            })

            res.end()
          } catch (e) {
            throw new Error(e)
          }
        },
      },
    }
  )
}

export default auth
