import Endpoint from "../classes/endpoint"

const community = (service, filters) => {
  const { checkMongoId, isBlocked } = filters

  return new Endpoint(
    // Name
    "message",

    // MAIN URL
    { pre: checkMongoId, url: "/message" },

    // HANDLER ROUTES
    {
      // send a message to a user
      send: {
        path: "/send/:id",
        method: "POST",
        pre: isBlocked,
        handler: async (req, res, next) => {
          try {
            const resMessage = await service.send(
              req.user.id,
              req.params.id,
              req.body
            )

            res.status(200).json({
              id: resMessage._id,
            })
          } catch (e) {
            next(e)
          }
        },
      },

      // delete a message
      delete: {
        path: "/delete/:id",
        method: "DELETE",
        handler: async (req, res, next) => {
          try {
            await service.delete(req.params.id, req.user.id)

            res
              .status(200)
              .json({ message: "Message was deleted successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // set a message as read
      read: {
        path: "/read/:id",
        method: "PUT",
        handler: async (req, res, next) => {
          try {
            await service.read(req.params.id)

            res.status(200).json({ message: "Message read was successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // get a conversation between 2 users
      conversation: {
        path: "/conversation/:id",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const messages = await service.conversation(
              req.user.id,
              req.params.id,
              req.query.limit
            )

            res.status(200).json(messages)
          } catch (e) {
            next(e)
          }
        },
      },
    }
  )
}

export default community
