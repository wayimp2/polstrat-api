import Endpoint from "../classes/endpoint"

const communityAnnouncement = (service, filters) => {
  const { checkMongoId, isOwner } = filters

  return new Endpoint(
    // Name
    "community-announcement",

    // MAIN URL
    { pre: isOwner, url: "/community/announcement" },

    // HANDLER ROUTES
    {
      // create a new announcement
      create: {
        path: "/create",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const announcement = await service.create(req.user.id, req.body)

            res.status(200).json({ id: announcement._id })
          } catch (e) {
            next(e)
          }
        },
      },

      // update an existing announcement
      update: {
        path: "/update/:id",
        method: "PUT",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            await service.update(req.params.id, req.body)

            res
              .status(200)
              .json({ message: "Community announcement updated successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // delete an existing announcement
      delete: {
        path: "/delete/:id",
        method: "DELETE",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            await service.delete(req.params.id)

            res
              .status(200)
              .json({ message: "Community announcement deleted successfully" })
          } catch (e) {
            next(e)
          }
        },
      },
    }
  )
}

export default communityAnnouncement
