import Endpoint from "../classes/endpoint"

const userConnection = (service, filters) => {
  const { checkMongoId, isBlocked } = filters

  return new Endpoint(
    // NAME
    "user-connection",

    // ENDPOINT URL
    { url: "/user/connection" },

    // ROUTES
    {
      // request user to connect
      request: {
        path: "/request/:id",
        method: "POST",
        pre: [checkMongoId, isBlocked],
        handler: async (req, res, next) => {
          try {
            await service.request(req.user.id, req.params.id)
            res.status(200).json({ message: "Request was successfull" })
          } catch (e) {
            next(e)
          }
        },
      },

      // approve connection to a user
      approve: {
        path: "/approve/:id",
        method: "PUT",
        pre: [checkMongoId, isBlocked],
        handler: async (req, res, next) => {
          try {
            await service.approve(req.user.id, req.params.id)
            res
              .status(200)
              .json({ message: "User connection approved successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // delete or reject connection
      reject: {
        path: "/reject/:id",
        method: "DELETE",
        pre: [checkMongoId, isBlocked],
        handler: async (req, res, next) => {
          try {
            await service.reject(req.user.id, req.params.id)
            res
              .status(200)
              .json({ message: "User connection rejected successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // block other user
      block: {
        path: "/block/:id",
        method: "POST",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            const resUserConnection = await service.block(
              req.user.id,
              req.params.id
            )
            res.status(200).json({ id: resUserConnection._id })
          } catch (e) {
            next(e)
          }
        },
      },

      // get all of my connecions
      my: {
        path: "/my",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const userConnections = await service.my(req.user.id, req.query)

            if (userConnections) {
              res.status(200).json(userConnections)
            } else {
              throw new Error("No user was found with that id")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // get all of my connecions
      countMy: {
        path: "/my/count",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const userConnections = await service.countMy(req.user.id)

            res.status(200).json(userConnections)
          } catch (e) {
            next(e)
          }
        },
      },

      // get all requests i have from another user
      requests: {
        path: "/requests",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const userConnections = await service.requests(
              req.user.id,
              req.query
            )

            if (userConnections) {
              res.status(200).json(userConnections)
            } else {
              throw new Error("No user was found with that id")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // get all of my connecions
      countRequests: {
        path: "/requests/count",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const userConnections = await service.countRequests(req.user.id)
            res.status(200).json(userConnections)
          } catch (e) {
            next(e)
          }
        },
      },

      // get all pending requests i have made to another user
      pending: {
        path: "/pending",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const userConnections = await service.pending(req.user.id)

            if (userConnections) {
              res.status(200).json(userConnections)
            } else {
              throw new Error("No user was found with that id")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // get all users ive blocked
      blocked: {
        path: "/blocked",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const userConnections = await service.blocked(req.user.id)

            if (userConnections) {
              res.status(200).json(userConnections)
            } else {
              throw new Error("No user was found with that id")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // Get connection between two users
      getConnection: {
        path: "/get/:id",
        method: "GET",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            if (!req.params.id) {
              throw new Error("No id was found.")
            }

            const userConnections = await service.getConnection(
              req.user.id,
              req.params.id
            )

            if (userConnections) {
              res.status(200).json(userConnections)
            } else {
              res.status(200).json({ status: 0 })
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // Get connection between two users
      getNotifications: {
        path: "/notifications",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const notifications = await service.getNotifications(req.user.id)

            res.status(200).json(notifications)
          } catch (e) {
            next(e)
          }
        },
      },

      // Get connection between two users
      getNewNotifications: {
        path: "/notifications/new",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const notifications = await service.getNewNotifications(req.user.id)

            res.status(200).json(notifications)
          } catch (e) {
            next(e)
          }
        },
      },

      // Get connection between two users
      notificationSeen: {
        path: "/notifications/seen/:id",
        method: "GET",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            if (!req.params.id) {
              throw new Error("No id was found.")
            }

            const notifications = await service.notificationSeen(req.params.id)

            res.status(200).json(notifications)
          } catch (e) {
            next(e)
          }
        },
      },
    }
  )
}

export default userConnection
