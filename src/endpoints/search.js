import Endpoint from "../classes/endpoint"

const search = service => {
  return new Endpoint(
    // NAME
    "search",

    // ENDPOINT OPTIONS
    { url: "/search" },

    // ROUTE HANDLER

    {
      // search by text string
      text: {
        path: "/text",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const searchResult = await service.text(req.query)

            res.status(200).json(searchResult)
          } catch (e) {
            next(e)
          }
        },
      },

      // search user by choosen filters
      userFilter: {
        path: "/user/filter",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const searchResult = await service.userFilter(req.body)

            res.status(200).json(searchResult)
          } catch (e) {
            next(e)
          }
        },
      },

      // search community by choosen filters
      communityFilter: {
        path: "/community/filter",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const searchResult = await service.CommunityFilter(req.body)

            res.status(200).json(searchResult)
          } catch (e) {
            next(e)
          }
        },
      },
    }

    // Add more here...
  )
}

export default search
