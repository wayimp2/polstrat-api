import Endpoint from "../classes/endpoint"

const app = () => {
  return new Endpoint(
    // NAME
    "app",

    // ENDPOINT OPTIONS
    {
      url: "/",
      //pre: (req, res, next) => next(),
      //post: (req, res, next) => next(),
    },

    // ROUTE HANDLER
    {
      healthCheck: {
        path: "/health-check",
        //pre: (req, res, next) => next()
        //post: (req, res, next) => next()
        //methods: ["GET"],
        method: "GET",
        handler: (req, res, next) => res.status(200).end(),
      },
    }

    // Add more here...
  )
}

export default app
