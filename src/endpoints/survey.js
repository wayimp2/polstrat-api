import Endpoint from '../classes/endpoint'

const survey= (service, filters) => {
  return new Endpoint(
    // Name
    'survey',

    // MAIN URL
    { url: '/survey' },

    // HANDLER ROUTES
    {
      create: {
        path: '/create',
        method: 'POST',
        handler: async (req, res, next) => {
          try {
            const survey = await service.create(req.user.id, req.body)
            res.status(200).json({ id: survey._id })
          } catch (e) {
            next(e)
          }
        }
      },

      update: {
        path: '/update/:id',
        method: 'PUT',
        handler: async (req, res, next) => {
          try {
            await service.update(req.params.id, req.body)
            res.json({ message: 'Updated successfully' })
          } catch (e) {
            next(e)
          }
        }
      },

      delete: {
        path: '/delete/:id',
        method: 'DELETE',
        handler: async (req, res, next) => {
          try {
            await service.delete(req.params.id)
            res.status(200).json({ message: 'Deleted successfully' })
          } catch (e) {
            next(e)
          }
        }
      },

      all: {
        path: '/all',
        method: 'GET',
        handler: async (req, res, next) => {
          try {
            const surveys = await service.all()
            res.status(200).json(surveys)
          } catch (e) {
            console.log(e)
            next(e)
          }
        }
      }
    }
  )
}

export default survey
