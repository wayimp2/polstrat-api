import Endpoint from "../classes/endpoint"

const community = (service, filters) => {
  const { checkMongoId, isOwner } = filters

  return new Endpoint(
    // Name
    "community",

    // MAIN URL
    { url: "/community" },

    // HANDLER ROUTES
    {
      // Get current community
      current: {
        path: "/current",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const community = await service.current(req.user.id)
            if (community) {
              res.status(200).json(community)
            } else {
              throw new Error("No community found")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // Create a new community
      create: {
        path: "/create",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            const community = await service.create(req.user.id, req.body)
            res.status(200).json({ id: community._id })
          } catch (e) {
            next(e)
          }
        },
      },

      // Update a community
      update: {
        path: "/update/:id",
        method: "PUT",
        pre: [checkMongoId, isOwner],
        handler: async (req, res, next) => {
          try {
            await service.update(req.params.id, req.body)
            res.json({ message: "Community updated successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // Delete a community
      delete: {
        path: "/delete/:id",
        method: "DELETE",
        pre: [checkMongoId, isOwner],
        handler: async (req, res, next) => {
          try {
            await service.delete(req.params.id)
            res.status(200).json({ message: "Community deleted successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // (temporary)
      all: {
        path: "/all",
        method: "GET",
        handler: async (req, res, next) => {
          const limit = req.query.limit

          try {
            const communities = await service.all(limit)

            if (communities && communities.length) {
              res.status(200).json(communities)
            } else {
              throw new Error("No communities found")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // Get community by alias (!important must be in the end of this chain)
      alias: {
        path: "/:alias",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const community = await service.getByAlias(req)

            if (community) {
              res.status(200).json(community)
            } else {
              res.status(404).json({
                message: "No community found with that username",
              })
            }
          } catch (e) {
            next(e)
          }
        },
      },
    }
  )
}

export default community
