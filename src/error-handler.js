const errorHandler = (err, req, res, next) => {
  const message = err.message

  if (typeof err === "string") {
    // custom application error
    return res.status(400).json({ message })
  }

  if (err.name === "ValidationError") {
    // mongoose validation error
    return res.status(400).json({ message })
  }

  if (err.name === "UnauthorizedError") {
    // jwt authentication error
    return res.status(401).json({ message })
  }

  // default to 500 server error
  console.log("500 Server Error", err)
  return res.status(500).json({ message })
}

export default errorHandler
