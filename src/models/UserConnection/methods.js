const methods = models => {
  const findOneAndDelete = {
    // HOOK
    type: "post",
    option: "findOneAndDelete",
    function: async userConnection => {
      const { Notification } = models

      try {
        console.log(models)
        console.log(userConnection)

        await Notification.findOneAndDelete({
          UserConnection: userConnection.id,
        })
      } catch (e) {
        throw new Error(e)
      }
    },
  }

  return [findOneAndDelete]
}

export default methods
