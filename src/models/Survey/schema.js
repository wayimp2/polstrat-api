const schema = ({ Schema }) => {
  return {
    label: {
      type: String,
    },
    value: {
      type: String,
    },
    sections: {
      type: [Object]
    }
  }
}

export default schema
