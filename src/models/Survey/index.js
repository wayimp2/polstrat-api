import schema from './schema'

const Survey = {
  name: 'Survey',
  schema: schema,
  options: {
    timestamps: true
  }
}

export default Survey
