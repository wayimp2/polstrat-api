import schema from "./schema"

const Announcement = {
  name: "Announcement",
  schema: schema,
  options: {
    timestamps: true,
  },
}

export default Announcement
