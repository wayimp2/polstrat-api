import schema from "./schema"

const Notification = {
  name: "Notification",
  schema: schema,
  options: {
    timestamps: true,
  },
}

export default Notification
