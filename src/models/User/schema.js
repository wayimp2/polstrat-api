import validator from "validator"
import {
  nationalities,
  languages,
  personalities,
  professions,
  religions,
} from "../../helpers"

const visibilityDefault = {
  type: Number,
  enum: [
    1, // private
    2, // friends
    3, // public
  ],
  default: 3,
}

const questionsDefault = {
  type: String,
  enum: [
    1, // "Yes"
    2, // "No"
    3, // "I don't know"
  ],
}

const smallText = [55, "Text is only alowed to 55 characters maximum."]
const mediumText = [120, "Text is only alowed to 120 characters maximum."]
const largeText = [255, "Text is only alowed to 255 characters maximum."]
const extraLargeText = [5000, "Text is only alowed to 5000 characters maximum."]

const schema = ({ Schema }) => {
  return {
    admin: {
      type: Boolean,
      default: false,
    },

    confirmed: {
      type: Boolean,
      default: false,
    },

    // Credentials
    alias: {
      type: String,
      trim: true,
      required: true,
      unique: true,
    },
    displayName: {
      type: String,
      maxlength: [20, "Username is alowed to 20 characters maximum"],
      minlength: [3, "Username must be at least 3 characters"],
      required: true,
      unique: true,
    },
    hash: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: "Email address is required",
      validate: [validator.isEmail, "Please fill a valid email address"],
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Please fill a valid email address",
      ],
    },

    lastLogin: Date,

    // Password attempts
    passwordAttempts: {
      type: Number,
      default: 0,
    },
    passwordLock: Date,

    // INFORMATION //////////////////////////////////////////////////////////////

    // user general informations
    information: {
      // Survey
      studyPlan: Boolean,
      studyPlanReason: {
        type: String,
        maxlength: largeText,
      },

      understandPlan: Boolean,
      understandPlanExplain: {
        type: String,
        maxlength: largeText,
      },

      // BASIC INFO

      sex: {
        type: String,
        enum: ["M", "F", "Other"],
      },

      age: Number,

      ageRange: {
        type: String,
        enum: ["0-17", "18-30", "31-45", "46-60", "61-75", "75+"],
      },

      nationality: {
        type: String,
        enum: nationalities,
      },

      languages: {
        type: [String],
        enum: languages,
      },

      romanticRelationship: {
        type: String,
        enum: ["Married", "Unmarried", "Single"],
      },

      haveChildren: Boolean,

      // LOCATION

      location: [
        {
          continent: {
            type: String,
            maxlength: smallText,
          },
          country: {
            type: String,
            maxlength: smallText,
          },
          region: {
            type: String,
            trim: true,
            maxlength: smallText,
          },
          city: {
            type: String,
            trim: true,
            maxlength: smallText,
          },
        },
      ],

      living: {
        type: String,
        enum: ["In the city", "Countryside", "In the middle of nowhere"],
      },

      // RELIGION

      religion: {
        type: String,
        enum: religions,
      },

      religiousPref: {
        type: [String],
      },

      religiousMatch: Boolean,
      halalKosherkRitual: Boolean,

      // POLITICAL

      politicalAllegiance: {
        type: String,
        trim: true,
        maxlength: smallText,
      },

      matchPolitical: Boolean,
      voteNationalElections: Boolean,
      politicalActivism: Boolean,

      // SEARCH

      searchCommunity: Boolean,
      lookingFriends: Boolean,
      willingToMove: Boolean,
      regionsToMove: {
        type: [String],
      },
      willingToRegroup: Boolean,
      likeMindedFriends: Boolean,

      // MENTAL AND PHYSICAL

      mentalShape: {
        type: String,
        enum: [
          "Good",
          "Perfect",
          "I'm Blackpilled",
          "So so",
          "I suffer from depression",
        ],
      },

      personalityTest: {
        type: String,
        enum: personalities,
      },

      physicalShape: {
        type: String,
        enum: ["Good", "Perfect", "I'm overweight", "So so", "Bad shape"],
      },

      physicalHealth: {
        type: String,
        enum: [
          "I'm fully healthy",
          "I cannot live without medications",
          "I'm crippled",
        ],
      },

      physicalHealthReason: {
        type: String,
        maxlength: mediumText,
      },

      // SKILLS

      skillsSpecialization: {
        type: String,
        trim: true,
        maxlength: mediumText,
      },

      willingToLearn: Boolean,
      willingToTeach: Boolean,

      typeOfTeach: {
        type: String,
        trim: true,
        maxlength: mediumText,
      },

      compensated: {
        type: String,
        enum: ["Labour", "Money", "Nothing"],
      },

      hobbiesAndInterests: {
        type: String,
        trim: true,
        maxlength: mediumText,
      },

      likeToLearn: {
        type: String,
        trim: true,
        maxlength: mediumText,
      },

      swim: Boolean,
      farmingExperience: Boolean,

      // JOB

      haveJob: Boolean,
      lookingForJob: Boolean,

      jobCategories: {
        type: [String],
        enum: professions,
      },

      ownAnyBusiness: Boolean,

      // TODO: limit by enum maybe
      chooseBusinessCategory: {
        type: String,
        maxlength: mediumText,
      },

      openToHire: Boolean,

      skillRequirements: {
        type: String,
        maxlength: mediumText,
      },

      willingToOperate: Boolean,

      // DRIVING SKILLS

      drive: Boolean,
      ownDrivingLicense: Boolean,

      lightLandVehicles: Boolean,
      heavyLandVehicles: Boolean,

      aircraft: Boolean,
      watercraft: Boolean,

      farmEquipment: Boolean,
      rideHorses: Boolean,

      otherVehicles: Boolean,

      writeOtherVehicles: {
        type: String,
        maxlength: mediumText,
      },

      // OTHER

      rentLivingPlace: Boolean,
      placesForVacationers: Boolean,

      ownLand: Boolean,
      howMuchLand: {
        type: String,
        maxlength: mediumText,
      },

      willingToRent: {
        type: String,
        maxlength: mediumText,
      },
      areInDebt: Boolean,

      areYouPrepper: Boolean,
      willingContributLand: Boolean,

      describeYourFarmlands: {
        type: String,
        maxlength: extraLargeText,
      },

      sayAboutYourself: {
        type: String,
        maxlength: extraLargeText,
      },
    },

    // RIBBONS //////////////////////////////////////////////////////////////

    // Search Ribbons (P5)
    ribbons: {
      1: Boolean,
      2: Boolean,
      3: Boolean,
      4: Boolean,
      5: Boolean,
      6: Boolean,
      7: Boolean,
      8: Boolean,
      9: Boolean,
      10: Boolean,
      11: Boolean,
      12: Boolean,
      13: Boolean,
      14: Boolean,
      15: Boolean,
      16: Boolean,
      17: Boolean,
      18: Boolean,
      19: Boolean,

      question1: {
        type: String,
        maxlength: largeText,
      },

      question2: {
        type: String,
        maxlength: largeText,
      },
    },

    // Requirements Questions (P6)
    questions: {
      workMuslims: questionsDefault,
      workChristians: questionsDefault,
      workJews: questionsDefault,
      workPagans: questionsDefault,
      workBuddhistsHindus: questionsDefault,
      workAtheists: questionsDefault,

      friendsMuslims: questionsDefault,
      friendsChristians: questionsDefault,
      friendsJews: questionsDefault,
      friendsPagans: questionsDefault,
      friendsBuddhistsHindus: questionsDefault,
      friendsAtheists: questionsDefault,
    },

    // Visibility options
    visibility: {
      // Hide/show all profile
      profile: visibilityDefault,

      // Hide/show all ribbons
      ribbons: visibilityDefault,

      // TODO: maybe add questions if needed
      // Hide/show all questions
      //questions: visibilityDefault,

      // simple options
      email: visibilityDefault,
      sex: visibilityDefault,
      age: visibilityDefault,
      ageRange: visibilityDefault,
      nationality: visibilityDefault,
      languages: visibilityDefault,
      resideCountry: visibilityDefault,
      location: visibilityDefault,
      locationRange: visibilityDefault,
      living: visibilityDefault,
      religion: visibilityDefault,
      religiousPref: visibilityDefault,
      religiousMatch: visibilityDefault,
      halalKosherkRitual: visibilityDefault,
      politicalAllegiance: visibilityDefault,
      matchPolitical: visibilityDefault,
      voteNationalElections: visibilityDefault,
      politicalActivism: visibilityDefault,
      searchCommunity: visibilityDefault,
      lookingFriends: visibilityDefault,
      willingToMove: visibilityDefault,
      regionsToMove: visibilityDefault,
      willingToRegroup: visibilityDefault,
      physicalShape: visibilityDefault,
      mentalShape: visibilityDefault,
      personalityTest: visibilityDefault,
      physicalHealth: visibilityDefault,
      physicalHealthReason: visibilityDefault,
      skillsSpecialization: visibilityDefault,
      willingToLearn: visibilityDefault,
      willingToTeach: visibilityDefault,
      typeOfTeach: visibilityDefault,
      compensated: visibilityDefault,
      hobbiesAndInterests: visibilityDefault,
      likeToLearn: visibilityDefault,
      haveJob: visibilityDefault,
      lookingForJob: visibilityDefault,
      jobCategories: visibilityDefault,
      ownAnyBusiness: visibilityDefault,
      chooseBusinessCategory: visibilityDefault,
      openToHire: visibilityDefault,
      skillRequirements: visibilityDefault,
      willingToOperate: visibilityDefault,
      romanticRelationship: visibilityDefault,
      haveChildren: visibilityDefault,
      likeMindedFriends: visibilityDefault,
      swim: visibilityDefault,
      drive: visibilityDefault,
      ownDrivingLicense: visibilityDefault,
      lightLandVehicles: visibilityDefault,
      heavyLandVehicles: visibilityDefault,
      aircraft: visibilityDefault,
      watercraft: visibilityDefault,
      farmEquipment: visibilityDefault,
      rideHorses: visibilityDefault,
      otherVehicles: visibilityDefault,
      writeOtherVehicles: visibilityDefault,
      rentLivingPlace: visibilityDefault,
      areInDebt: visibilityDefault,
      farmingExperience: visibilityDefault,
      ownLand: visibilityDefault,
      howMuchLand: visibilityDefault,
      willingToRent: visibilityDefault,
      placesForVacationers: visibilityDefault,
      areYouPrepper: visibilityDefault,
      willingContributLand: visibilityDefault,
      sayAboutYourself: visibilityDefault,
    },

    // RELATIONSHIPS

    // Community only
    Community: { type: Schema.Types.ObjectId, ref: "Community" },
  }
}

export default schema
