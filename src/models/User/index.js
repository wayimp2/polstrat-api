import schema from "./schema"
import methods from "./methods"

const User = {
  name: "User",
  schema: schema,
  methods: methods,
  options: { timestamps: true },
}

export default User
