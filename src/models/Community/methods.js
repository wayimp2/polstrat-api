const methods = ({ models }) => {
  const findOneAndDelete = {
    // HOOK
    type: "post",
    option: "findOneAndDelete",
    function: async community => {
      const { User, CommunityUser } = models

      try {
        // Remove the Community id in the owner
        await User.updateMany(
          { Community: community.id },
          { $unset: { Community: true } }
        )

        // Remove all links in CommunityUsers
        await CommunityUser.deleteMany({
          Community: community.id,
        })
      } catch (e) {
        console.log(e)
        throw new Error(e)
      }
    },
  }

  const indexes = {
    // INDEXING
    type: "index",
    content: {
      alias: "text",
      displayName: "text",
      tag: "text",
      religiousAllowed: "text",
      politicalAllowed: "text",
      description: "text",
      business: "text",
      assetsAbout: "text",
    },
    options: {
      weights: {
        alias: 2,
        displayName: 1,
        tag: 1,
        religiousAllowed: 4,
        politicalAllowed: 4,
        description: 5,
        business: 3,
        assetsAbout: 5,
      },
    },
  }

  return [indexes, findOneAndDelete]
}

export default methods
