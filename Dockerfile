FROM node:12.3.1-alpine

WORKDIR /usr/src/app

COPY package*.json ./

COPY .env ./

RUN apk add --no-cache --virtual .gyp python make g++ \
  && npm install --production \
  && apk del .gyp

COPY ./build ./build

EXPOSE 9091

CMD [ "npm", "run", "serve" ]